SmilEditor
==========

This is a non-finished SMIL simple editor.

It's a linear editor with preview capabilities and it's focused in a week format schedule.

It's Java with Swing and the old JMF (Java Media Framework). The last one was used for metadata extraction and video preview. JMF uses _fobs4jmf_ for the support of modern codecs through _ffmpeg_. 

The package _communications_ contains some connection adaptors to different systems, some of them are finished, other not so much.

The code is rude and the project is not well-formed, it is my first Java app, Im sorry if you are going to take a look to the code.

Links
-----
JMF - Java Media Framework http://www.oracle.com/technetwork/java/javase/tech/index-jsp-140239.html

Fobs4JMF - http://fobs.sourceforge.net/

Authors
-------
Francisco José Moreno Llorca - packo@assamita.net - @kotejante

Francisco Jesús González Mata - chuspb@gmail.com - @chuspb

Third parties
-------------

jmf.jar & jmf.properties & nimbus.jar - JavaTM Media Framework (JMF) 2.1.x Binary Code License Agreement

fobs4jmf.jar -  LGPL 2.1

log4j - Apache License, Version 2.0

nimrodlf - LGPL 2.1

License
-------

    (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
    Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

