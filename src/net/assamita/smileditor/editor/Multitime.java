/*
 * Multitime.java
 *
 * Created on 28 de agosto de 2007, 12:45
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.assamita.smileditor.editor;

/**
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */
public class Multitime {
    
    /**
     * Creates a new instance of Multitime
     */
    public Multitime(int sec) {
        seconds = sec;
        DAY_MAX_TIME = ClipCollection.DAY_MAX_TIME;
    }
    
    public Multitime(){
        seconds = -1;
        DAY_MAX_TIME = ClipCollection.DAY_MAX_TIME;
    }
    
    public void setPixels(long pix,long max_time){
        seconds = (int)(((long)pix * (long)SECWEEK) / (long)max_time);
    }
    
    public void setTime(long sec){
        seconds = sec;        
    }
    
    public long getSeconds(){
        return seconds;
    }
   
    public int getPixels(int max_time){
        return (int)(((long)seconds * (long)max_time) / (long)SECWEEK);
    }
    
    public int getPixels(){
        int max_time = ClipCollection.DAY_MAX_TIME;
        return (int)(((long)seconds * (long)max_time) / (long)SECWEEK);
    }
    
    public String toString(){
        return Long.toString(seconds);
    }
    
    private long seconds;
    //SECWEEK: seconds in a week
    public static final int SECWEEK = 604800;
    //daytime: number of secons in a day;
    private final int DAYTIME=86400;
    private int DAY_MAX_TIME;

}
