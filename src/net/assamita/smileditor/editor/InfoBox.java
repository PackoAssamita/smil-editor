/*
 * MsgBox.java
 *
 * Created on 8 de marzo de 2007, 13:55
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;

/**
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

class InfoBox extends Dialog implements ActionListener {
    boolean id = false;
    Button ok,can;
    
    InfoBox(Frame frame, String msg, boolean okcan){
        super(frame, "Message", true);

        setLayout(new BorderLayout());
        JLabel label;
        label = new javax.swing.JLabel();
        label.setText(msg);
        label.setIcon(new javax.swing.ImageIcon("info.png"));
        add("Center",label);
        addOKCancelPanel(okcan);
        createFrame();
        pack();
        java.awt.Toolkit t = getToolkit();
        Image imagen = t.createImage("icono.gif");
        this.setIconImage(imagen);
        setVisible(true);
    }
    
    void addOKCancelPanel( boolean okcan ) {
        Panel p = new Panel();
        p.setLayout(new FlowLayout());
        createOKButton( p );
        if (okcan == true)
            createCancelButton( p );
        add("South",p);
    }
    
    void createOKButton(Panel p) {
        p.add(ok = new Button("Aceptar"));
        ok.addActionListener(this);
    }
    
    void createCancelButton(Panel p) {
        p.add(can = new Button("Cancelar"));
        can.addActionListener(this);
    }
    
    void createFrame() {
        Dimension d = getToolkit().getScreenSize();
        setLocation(d.width/3,d.height/3);
    }
    
    public void actionPerformed(ActionEvent ae){
        if(ae.getSource() == ok) {
            id = true;
            setVisible(false);
        } else if(ae.getSource() == can) {
            setVisible(false);
        }
    }
}