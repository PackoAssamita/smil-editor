/*
 * SmilWriter.java
 *
 * Created on 27 de julio de 2007, 14:29
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
(C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author packo
 */
public class SmilWriter {
    
    /**
     * Creates a new instance of SmilWriter
     */
    public SmilWriter(int action) {
        platform = System.getProperty( "os.name" );
        filepath = getTmpPath();
        mtime = new Multitime();
        DAY_MAX_TIME = ClipCollection.DAY_MAX_TIME;
        ACTION = action;
    }
    
    public SmilWriter(String path, int action){
        platform = System.getProperty( "os.name" );
        filepath = path;
        mtime = new Multitime();
        DAY_MAX_TIME = ClipCollection.DAY_MAX_TIME;
        ACTION = action;
    }
    
    public void addElement(Clip clipy, boolean consecutive){
         int dur=0,duration=0;
         int times,module,sec_start=0;
         boolean rest = false, bstart= false, rep = false;
         //mtime.setTime(clipy.getDuration());
         
         //startClip
         sstartClip = " startClip=\"";
         if (!clipy.isRedStart())
             sstartClip = "";
         else{
             //REVISAR
             mtime.setPixels(clipy.getRedStart() ,DAY_MAX_TIME);
             sstartClip += Integer.toString((int)mtime.getSeconds()) + "\"";
             sec_start = (int)mtime.getSeconds();
             bstart = true;
         }
         
         // dur
         send = " end=\"";
         if (!clipy.isRedWidth())
             //dur = 0;
             send = "";
         else{
             rep = true;
             //Multitime swtime = new Multitime();
             duration = clipy.getDuration();
             mtime.setPixels(clipy.getWidth(),DAY_MAX_TIME);
             dur = (int)mtime.getSeconds();
             // for repcount:
             if (dur > duration-sec_start){
                 times =  dur / (duration-sec_start);
                 module = dur % (duration-sec_start);
                 if (module!= 0){
                    smodule = send+ Integer.toString(module) + "\"";
                     rest = true;
                 }
                 send = " repCount=\""+Integer.toString(times)+"\"";
                 //send += += Integer.toString((int)mtime.getSeconds()) + "s\"";
//                REVISAR
//                if (bstart){
//                    times -= 1;
//                }
             }
             else
                send += Integer.toString((int)mtime.getSeconds()) + "\"";
         }
         
         // begin
         sbegin = " begin=\"";
         if(consecutive)
             sbegin = "";
         else{
             mtime.setPixels(clipy.getStart(),DAY_MAX_TIME);
             sbegin += Integer.toString((int)mtime.getSeconds())+ "\"";
         }
         
         
         // ruta
         String[] ruta;
         String sruta="";
         if (ACTION==SEND){
            ruta = clipy.getName().split("/");
            sruta = ruta[ruta.length-1];
         }
         else if (ACTION==SAVE)
             sruta = clipy.getName();
         //if repeat
         if ( rep){
             // and there is a seek at begining
             if(bstart){
                   out.println("          <video id=\""+clipy.getID()+"\" src=\""+sruta+"\""+ sbegin + sstartClip + " region=\"background\" fit=\"fill\"/>");
                    out.println("          <video id=\""+clipy.getID()+"\" src=\""+sruta+"\""+ send +" region=\"background\" fit=\"fill\"/>");
             }
             
             else{
                 out.println("          <video id=\""+clipy.getID()+"\" src=\""+sruta+"\""+ sbegin + sstartClip  +" region=\"background\" fit=\"fill\"/>");
                 out.println("          <video id=\""+clipy.getID()+"\" src=\""+sruta+"\""+ send +" region=\"background\" fit=\"fill\"/>");
             }
            if(rest)
                out.println("          <video id=\""+clipy.getID()+"\" src=\""+sruta+"\""+ smodule +" region=\"background\" fit=\"fill\"/>");
         }
         else{
             
            out.println("          <video id=\""+clipy.getID()+"\" src=\""+sruta+"\""+ sbegin + sstartClip + send +" region=\"background\" fit=\"fill\"/>");
         }
    }
    
    public void newDocument(String title, String generator, String author){
         String ENCODING = "ISO-8859-1";
         try{
            out = new PrintWriter(new FileOutputStream(filepath));
         }catch(java.io.FileNotFoundException e){
             
         }
         
        out.println("<?xml version=\"1.0\" encoding=\""+ENCODING+"\"?>");
        out.println("<!DOCTYPE smil PUBLIC \"-//W3C//DTD SMIL 2.0//EN\" \"http://www.w3.org/2001/SMIL20/SMIL20.dtd\">");
        out.println("<smil xmlns=\"http://www.w3.org/2001/SMIL20/Language\">");
        out.println("   <head>");
        out.println("       <meta name=\"title\" content=\""+title+"\"/>");
        out.println("       <meta name=\"generator\" content=\""+generator +"\"/> ");
        out.println("       <meta name=\"author\" content=\""+author+"\"/>");
        out.println("       <layout>");
        out.println("           <root-layout id=\"background\" backgroundColor=\"gray\" width=\"800\" height=\"600\"/>");
        out.println("       </layout>");
        out.println("   </head>");
        out.println("<body>");
        out.println("   <seq>");
        //out.close();
    }
    
    public void closeDocument(){
        out.println("   </seq>");
        out.println("</body>");
        out.println("</smil>");
        out.close();
    }
    
    public String getTmpPath(){
        
        // creating a temporary file.
        File td = new File("./tmp");
        td.mkdir();
        //tmp_path = td.getAbsolutePath();
        return td.getAbsolutePath()+"/playlist.smil";
    }
    
    private String filepath;
    private String platform;    
    private static PrintWriter out;
    private Multitime mtime;
    private String send ,sbegin ,sstartClip,smodule;
    //private String title;
    //private String generator;
    //private String author;
    private int DAY_MAX_TIME;
    public static int SAVE = 0;
    public static int SEND = 1;
    private int ACTION;
}
