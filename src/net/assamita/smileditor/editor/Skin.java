/*
 * Skin.java
 *
 * Created on 7 de septiembre de 2007, 10:10
 * 
(C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.assamita.smileditor.editor;

import java.awt.*;
/**
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */
public class Skin {
    
    /** Creates a new instance of Skin */
    public Skin() {
        MONDAY = new Color(213,63,63);
        TUESDAY = new Color(215,107,191);
        WEDNESDAY = new Color(107,215,160);
        THURSDAY = new Color(209,215,107);
        FRIDAY = new Color(115,107,215);
        SATURDAY = new Color(112,217,225);
        SUNDAY = new Color(112,225,125);
        BACKGROUND = new Color(214,223,136);
    }
    
    public Color getWeekColor(int index){
        switch(index){
            case 0:
                return MONDAY;
            case 1:
                return TUESDAY;
            case 2:
                return WEDNESDAY;
            case 3:
                return THURSDAY;
            case 4:
                return FRIDAY;
            case 5:
                return SATURDAY;
            case 6:
                return SUNDAY;
            default:
                return Color.BLACK;
        }
    }
    
    public Color MONDAY;
    public Color TUESDAY;
    public Color WEDNESDAY;
    public Color THURSDAY;
    public Color FRIDAY;
    public Color SATURDAY;
    public Color SUNDAY;
    public Color BACKGROUND;
    
}
