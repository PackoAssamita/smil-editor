/*
 * clipJMF.java
 *
 * Created on 1 de marzo de 2007, 17:24
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;

import javax.media.*;
import com.sun.media.ui.*;
import javax.media.protocol.*;
import javax.media.protocol.DataSource;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.Vector;


/**
 *  is a JInternalFrame with a media player from JMF
 *
 * @author packo
 */
class PlayerJMF extends JInternalFrame implements ControllerListener {
    Player mplayer;
    Component visual = null;
    Component control = null;
    int videoWidth = 0;
    int videoHeight = 0;
    int controlHeight = 30;
    int insetWidth = 10;
    int insetHeight = 30;
    boolean firstTime = true;
    
    public PlayerJMF(String filename) {
        //super(filename, true, true, true, true);
        String mediaFile = filename;
	Player player = null;
        title = mediaFile;
        duration = new Double(0);
	// URL for our media file
	URL url = null;
	try {
	    // Create an url from the file name and the url to the
	    // document containing this applet.
	    if ((url = new URL(mediaFile)) == null) {
		//Fatal("Can't build URL for " + mediaFile);
		return;
	    }
	    // Create an instance of a player for this media
	    try {
		player = Manager.createPlayer(url);   
	    } catch (NoPlayerException e) {
		//Fatal("Error: " + e);
	    }
	} catch (MalformedURLException e) {
	    //Fatal("Error:" + e);
            System.out.println("Error: " + e);
	} catch (IOException e) {
	    //Fatal("Error:" + e);
            System.out.println("Error: " + e);
	}
	if (player != null) {
	    this.mediaFile = mediaFile;
	   // JMFrame jmframe = new JMFrame(player, filename);
	    //add(jmframe);
	}
        
        duration = Double.valueOf(player.getDuration().getSeconds());
	
	getContentPane().setLayout( new BorderLayout() );
	//setSize(320, 10);
	setLocation(0, 0);
	setVisible(true);
	mplayer = player;        
	mplayer.addControllerListener((ControllerListener) this);
	mplayer.realize();
        
	addInternalFrameListener( new InternalFrameAdapter() {
	    public void internalFrameClosing(InternalFrameEvent ife) {
		mplayer.close();
	    }
	} );
		    
    }
    
    /**
     * Show a message, generally an error. Need com.sun.media.ui.*
     *
     * @param s         The message to show.
     */
    static void Fatal(String s) {
        MsgBox mb = new MsgBox(new Frame(),s,false);
    }
    
    /**
     * get the duration of playing media
     *
     * @return the duration of playing media
     */
    public int getDuration(){
        return duration.intValue();
    }
    
    public void stopMedia(){
        mplayer.stop();
        mplayer.close();
       // mplayer.deallocate();
    }
    
    public void controllerUpdate(ControllerEvent ce) {
	if (ce instanceof RealizeCompleteEvent) {
	    mplayer.prefetch();
	} else if (ce instanceof PrefetchCompleteEvent) {
	    if (visual != null)
		return;
	    
	    if ((visual = mplayer.getVisualComponent()) != null) {
		Dimension size = visual.getPreferredSize();
		videoWidth = size.width;
		videoHeight = size.height;
		getContentPane().add("Center", visual);
	    } else
		videoWidth = 320;
	    if ((control = mplayer.getControlPanelComponent()) != null) {
		controlHeight = control.getPreferredSize().height;
		getContentPane().add("South", control);
	    }
	    //setSize(videoWidth + insetWidth,videoHeight + controlHeight + insetHeight);
	    validate();
	    mplayer.start();
	} else if (ce instanceof EndOfMediaEvent) {
	    mplayer.setMediaTime(new Time(0));
	    mplayer.start();
	}
    }
    private static String mediaFile;
    private static Double duration;
}
