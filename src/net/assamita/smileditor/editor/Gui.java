/*
 * Gui.java
 *
 * Created on 19 de febrero de 2007, 12:39
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */
package net.assamita.smileditor.editor;

import com.omnividea.media.renderer.video.Java2DRenderer;
import java.awt.*;
import javax.swing.*;
import java.io.*;
import org.jdesktop.swingx.plaf.nimbus.*;
import com.nilo.plaf.nimrod.*;
/**
 * 
 * Main class of project.
 * Create the interfaz and connect all components.
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */
public class Gui extends javax.swing.JFrame {
    
    /**
     * Creates new form Gui
     */
    public Gui() {
        
        initComponents();
        b_gettingTemplate.setVisible(false);
        //initComponents2();
        setLocationRelativeTo(null);
        aread = new DibujoArea(pPlayer,this);
        jSlider1.setMaximum(aread.getColeccion().MAX_VALUE_SLIDER1);
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addComponent(aread, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addComponent(aread, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE)
        );

        changeLook(actuallook);
        
        java.awt.Toolkit t = getToolkit();
        Image imagen = t.createImage("icono.gif");
        this.setIconImage(imagen);
        w_openfile.setIconImage(imagen);
        w_writefile.setIconImage(imagen);
        Image imageninfo = t.createImage("gtk-about.png");
        w_acercade.setIconImage(imageninfo);
        w_preferences.setIconImage(imagen);
        w_gettingTemplate.setIconImage(imagen);
        
        
        semana = new String[] {"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
        semana_buttons = new JToggleButton[] {tb_lunes,tb_martes,tb_miercoles,tb_jueves,tb_viernes,tb_sabado,tb_domingo};
        
        c_lunes = new Color(213,63,63);
        c_martes = new Color(215,107,191);
        c_miercoles = new Color(107,215,160);
        c_jueves = new Color(209,215,107);
        c_viernes = new Color(115,107,215);
        c_sabado = new Color(112,217,225);
        c_domingo = new Color(112,225,125);
        
        tb_lunes.setBackground(c_lunes);
        tb_martes.setBackground(c_martes);
        tb_miercoles.setBackground(c_miercoles);
        tb_jueves.setBackground(c_jueves);
        tb_viernes.setBackground(c_viernes);
        tb_sabado.setBackground(c_sabado);
        tb_domingo.setBackground(c_domingo);
        //actual_day = 0;
        
        //filter:
        javax.swing.filechooser.FileFilter fileFilter = new SmilFilter();
        f_openfile.setFileFilter(fileFilter);
        f_writefile.setFileFilter(fileFilter);
        Dimension d = getToolkit().getScreenSize();
        w_openfile.setLocation(d.width/3,d.height/3);
        w_writefile.setLocation(d.width/3,d.height/3);
        w_acercade.setLocation(d.width/3,d.height/3);
        w_preferences.setLocation(d.width/3,d.height/3);
        w_senddata.setLocation(d.width/3,d.height/3);
        w_crew.setLocation(d.width/3,d.height/3);
        w_login.setLocation(d.width/3,d.height/3);
        w_gettingTemplate.setLocation(d.width/3,d.height/3);
        //this.setLocation(d.width/3,d.height/3);
        
        this.setSize(1050,650);
        aread.newMark(0);
        this.setVisible(false);
        w_gettingTemplate.setVisible(true);
   //     w_gettingTemplate.setSize(400,300);
        
//        fileFilter.
    }
    
     private void initComponents2() {
        jFileChooser1 = new javax.swing.JFileChooser();
        jPanel1 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        aread = new DibujoArea(pPlayer,this);
        

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jFileChooser1.setControlButtonsAreShown(false);
        jFileChooser1.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
        jFileChooser1.setDragEnabled(true);

        //jPanel1.setBackground(new java.awt.Color(0, 255, 153));
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        
        

        jMenu1.setText("Menu");
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(169, 169, 169)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pack();
    }// </editor-fold>       
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        semanaGroup = new javax.swing.ButtonGroup();
        w_acercade = new javax.swing.JFrame();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        w_openfile = new javax.swing.JDialog();
        f_openfile = new javax.swing.JFileChooser();
        w_writefile = new javax.swing.JDialog();
        f_writefile = new javax.swing.JFileChooser();
        w_preferences = new javax.swing.JDialog();
        t_padre = new javax.swing.JTabbedPane();
        t_apparence = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        b_default = new javax.swing.JRadioButton();
        b_swing = new javax.swing.JRadioButton();
        b_nimbus = new javax.swing.JRadioButton();
        t_options = new javax.swing.JPanel();
        b_pref_cancel = new javax.swing.JButton();
        b_pref_accept = new javax.swing.JButton();
        lookGroup = new javax.swing.ButtonGroup();
        w_senddata = new javax.swing.JFrame();
        l_senddata = new javax.swing.JLabel();
        jPanel_sending_iso = new javax.swing.JPanel();
        l_iconsending = new javax.swing.JLabel();
        p_sending = new javax.swing.JProgressBar();
        b_cancel_send = new javax.swing.JButton();
        w_crew = new javax.swing.JDialog();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        if_importing = new javax.swing.JInternalFrame();
        jLabel13 = new javax.swing.JLabel();
        w_login = new javax.swing.JFrame();
        pf_passwd = new javax.swing.JPasswordField();
        l_passwd = new javax.swing.JLabel();
        b_passwd = new javax.swing.JButton();
        b_passwd_cancel = new javax.swing.JButton();
        w_gettingTemplate = new javax.swing.JDialog();
        l_gettingTemplate = new javax.swing.JLabel();
        pb_gettingTemplate = new javax.swing.JProgressBar();
        b_gettingTemplate = new javax.swing.JButton();
        jFileChooser1 = new javax.swing.JFileChooser();
        jSlider1 = new javax.swing.JSlider();
        jPanel1 = new javax.swing.JPanel();
        pPlayer = new javax.swing.JDesktopPane();
        tb_lunes = new javax.swing.JToggleButton();
        tb_martes = new javax.swing.JToggleButton();
        tb_miercoles = new javax.swing.JToggleButton();
        tb_jueves = new javax.swing.JToggleButton();
        tb_viernes = new javax.swing.JToggleButton();
        tb_sabado = new javax.swing.JToggleButton();
        tb_domingo = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jSlider2 = new javax.swing.JSlider();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        i_nuevo = new javax.swing.JMenuItem();
        i_abrir = new javax.swing.JMenuItem();
        i_guardar = new javax.swing.JMenuItem();
        i_guardarcopia = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        i_preferences = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        i_salir = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        i_send = new javax.swing.JMenuItem();
        i_burn = new javax.swing.JMenuItem();
        i_usb = new javax.swing.JMenuItem();
        i_mixed = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        i_informacion = new javax.swing.JMenuItem();
        i_changeProxy = new javax.swing.JMenuItem();
        i_horarios = new javax.swing.JMenuItem();
        i_tvswitch = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();

        w_acercade.setTitle("Acerca De");
        w_acercade.setAlwaysOnTop(true);
        w_acercade.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        w_acercade.setLocationByPlatform(true);
        w_acercade.setMinimumSize(new java.awt.Dimension(300, 170));
        w_acercade.setName("f_acercade"); // NOI18N
        w_acercade.setResizable(false);

        jLabel3.setText("packo@assamita.net");

        javax.swing.GroupLayout w_acercadeLayout = new javax.swing.GroupLayout(w_acercade.getContentPane());
        w_acercade.getContentPane().setLayout(w_acercadeLayout);
        w_acercadeLayout.setHorizontalGroup(
            w_acercadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_acercadeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, w_acercadeLayout.createSequentialGroup()
                .addContainerGap(85, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78))
        );
        w_acercadeLayout.setVerticalGroup(
            w_acercadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_acercadeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        w_openfile.setTitle("Abre archivo SMIL");
        w_openfile.setMinimumSize(new java.awt.Dimension(550, 350));

        f_openfile.setDialogTitle("Abrir SMIL");
        f_openfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f_openfileActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout w_openfileLayout = new javax.swing.GroupLayout(w_openfile.getContentPane());
        w_openfile.getContentPane().setLayout(w_openfileLayout);
        w_openfileLayout.setHorizontalGroup(
            w_openfileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(f_openfile, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        w_openfileLayout.setVerticalGroup(
            w_openfileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(f_openfile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        w_writefile.setMinimumSize(new java.awt.Dimension(550, 350));

        f_writefile.setDialogTitle("Guardar SMIL");
        f_writefile.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        f_writefile.setToolTipText("Elige un directorio para guardar");
        f_writefile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f_writefileActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout w_writefileLayout = new javax.swing.GroupLayout(w_writefile.getContentPane());
        w_writefile.getContentPane().setLayout(w_writefileLayout);
        w_writefileLayout.setHorizontalGroup(
            w_writefileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(f_writefile, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
        );
        w_writefileLayout.setVerticalGroup(
            w_writefileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(f_writefile, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
        );

        w_preferences.setTitle("Preferencias");
        w_preferences.setMinimumSize(new java.awt.Dimension(550, 400));

        jLabel4.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        jLabel4.setText("Temas");

        lookGroup.add(b_default);
        b_default.setSelected(true);
        b_default.setText("Predeterminado");
        b_default.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        b_default.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_defaultActionPerformed(evt);
            }
        });

        lookGroup.add(b_swing);
        b_swing.setText("Swing");
        b_swing.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        b_swing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_swingActionPerformed(evt);
            }
        });

        lookGroup.add(b_nimbus);
        b_nimbus.setText("Nimbus");
        b_nimbus.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        b_nimbus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_nimbusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout t_apparenceLayout = new javax.swing.GroupLayout(t_apparence);
        t_apparence.setLayout(t_apparenceLayout);
        t_apparenceLayout.setHorizontalGroup(
            t_apparenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(t_apparenceLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(t_apparenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(b_default)
                    .addComponent(b_swing)
                    .addComponent(b_nimbus))
                .addContainerGap(256, Short.MAX_VALUE))
        );
        t_apparenceLayout.setVerticalGroup(
            t_apparenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(t_apparenceLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(b_default)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(b_swing)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(b_nimbus)
                .addContainerGap(140, Short.MAX_VALUE))
        );

        t_padre.addTab("Apariencia", t_apparence);

        javax.swing.GroupLayout t_optionsLayout = new javax.swing.GroupLayout(t_options);
        t_options.setLayout(t_optionsLayout);
        t_optionsLayout.setHorizontalGroup(
            t_optionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 383, Short.MAX_VALUE)
        );
        t_optionsLayout.setVerticalGroup(
            t_optionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 230, Short.MAX_VALUE)
        );

        t_padre.addTab("Opciones Varias", t_options);

        b_pref_cancel.setText("Cancelar");
        b_pref_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_pref_cancelActionPerformed(evt);
            }
        });

        b_pref_accept.setText("Aceptar");
        b_pref_accept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_pref_acceptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout w_preferencesLayout = new javax.swing.GroupLayout(w_preferences.getContentPane());
        w_preferences.getContentPane().setLayout(w_preferencesLayout);
        w_preferencesLayout.setHorizontalGroup(
            w_preferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_preferencesLayout.createSequentialGroup()
                .addGroup(w_preferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(w_preferencesLayout.createSequentialGroup()
                        .addContainerGap(214, Short.MAX_VALUE)
                        .addComponent(b_pref_accept)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(b_pref_cancel))
                    .addComponent(t_padre, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE))
                .addContainerGap())
        );
        w_preferencesLayout.setVerticalGroup(
            w_preferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, w_preferencesLayout.createSequentialGroup()
                .addComponent(t_padre, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(w_preferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_pref_cancel)
                    .addComponent(b_pref_accept))
                .addContainerGap())
        );

        t_padre.getAccessibleContext().setAccessibleName("Preferencias");

        w_senddata.setTitle("Mandando datos");
        w_senddata.setMinimumSize(new java.awt.Dimension(360, 280));

        l_senddata.setText("Mandando información");

        b_cancel_send.setText("Cancelar");

        javax.swing.GroupLayout jPanel_sending_isoLayout = new javax.swing.GroupLayout(jPanel_sending_iso);
        jPanel_sending_iso.setLayout(jPanel_sending_isoLayout);
        jPanel_sending_isoLayout.setHorizontalGroup(
            jPanel_sending_isoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_sending_isoLayout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(b_cancel_send)
                .addContainerGap(143, Short.MAX_VALUE))
            .addGroup(jPanel_sending_isoLayout.createSequentialGroup()
                .addGap(132, 132, 132)
                .addComponent(p_sending, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(126, Short.MAX_VALUE))
            .addGroup(jPanel_sending_isoLayout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(l_iconsending, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(118, Short.MAX_VALUE))
        );
        jPanel_sending_isoLayout.setVerticalGroup(
            jPanel_sending_isoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_sending_isoLayout.createSequentialGroup()
                .addContainerGap(137, Short.MAX_VALUE)
                .addComponent(l_iconsending)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(p_sending, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(b_cancel_send)
                .addContainerGap())
        );

        javax.swing.GroupLayout w_senddataLayout = new javax.swing.GroupLayout(w_senddata.getContentPane());
        w_senddata.getContentPane().setLayout(w_senddataLayout);
        w_senddataLayout.setHorizontalGroup(
            w_senddataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(l_senddata, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
            .addGroup(w_senddataLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel_sending_iso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        w_senddataLayout.setVerticalGroup(
            w_senddataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_senddataLayout.createSequentialGroup()
                .addComponent(l_senddata)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jPanel_sending_iso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        w_crew.setMinimumSize(new java.awt.Dimension(600, 650));

        jLabel5.setText("Developer");

        jLabel7.setText("Project Team");

        jLabel9.setText("Supervisor");

        jLabel11.setText("Interfaz assistant");

        javax.swing.GroupLayout w_crewLayout = new javax.swing.GroupLayout(w_crew.getContentPane());
        w_crew.getContentPane().setLayout(w_crewLayout);
        w_crewLayout.setHorizontalGroup(
            w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_crewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, w_crewLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 463, Short.MAX_VALUE)
                        .addComponent(jLabel7))
                    .addGroup(w_crewLayout.createSequentialGroup()
                        .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(w_crewLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(184, 184, 184))
                            .addGroup(w_crewLayout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                                .addContainerGap())))
                    .addGroup(w_crewLayout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        w_crewLayout.setVerticalGroup(
            w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_crewLayout.createSequentialGroup()
                .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel5))
                .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(w_crewLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(w_crewLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(w_crewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE))
                .addContainerGap())
        );

        if_importing.setVisible(true);

        jLabel13.setText("Importando lista de reproducción");

        javax.swing.GroupLayout if_importingLayout = new javax.swing.GroupLayout(if_importing.getContentPane());
        if_importing.getContentPane().setLayout(if_importingLayout);
        if_importingLayout.setHorizontalGroup(
            if_importingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(if_importingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addContainerGap(195, Short.MAX_VALUE))
        );
        if_importingLayout.setVerticalGroup(
            if_importingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(if_importingLayout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addComponent(jLabel13)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        w_login.setMinimumSize(new java.awt.Dimension(280, 150));

        l_passwd.setText("Introduzca la contraseña:");

        b_passwd.setText("Aceptar");
        b_passwd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_passwdActionPerformed(evt);
            }
        });

        b_passwd_cancel.setText("Cancelar");
        b_passwd_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_passwd_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout w_loginLayout = new javax.swing.GroupLayout(w_login.getContentPane());
        w_login.getContentPane().setLayout(w_loginLayout);
        w_loginLayout.setHorizontalGroup(
            w_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_loginLayout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(w_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pf_passwd, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                    .addComponent(l_passwd))
                .addGap(73, 73, 73))
            .addGroup(w_loginLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(b_passwd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(b_passwd_cancel)
                .addContainerGap(59, Short.MAX_VALUE))
        );
        w_loginLayout.setVerticalGroup(
            w_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_loginLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(l_passwd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pf_passwd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(w_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_passwd)
                    .addComponent(b_passwd_cancel))
                .addContainerGap())
        );

        w_gettingTemplate.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        w_gettingTemplate.setAlwaysOnTop(true);
        w_gettingTemplate.setIconImage(null);
        w_gettingTemplate.setLocationByPlatform(true);
        w_gettingTemplate.setMinimumSize(new java.awt.Dimension(400, 150));
        w_gettingTemplate.setName("GettingTemplate"); // NOI18N

        l_gettingTemplate.setText("Recibiendo plantilla de contenidos...");

        pb_gettingTemplate.setIndeterminate(true);

        b_gettingTemplate.setText("Comprendido");
        b_gettingTemplate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_gettingTemplateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout w_gettingTemplateLayout = new javax.swing.GroupLayout(w_gettingTemplate.getContentPane());
        w_gettingTemplate.getContentPane().setLayout(w_gettingTemplateLayout);
        w_gettingTemplateLayout.setHorizontalGroup(
            w_gettingTemplateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_gettingTemplateLayout.createSequentialGroup()
                .addGap(107, 107, 107)
                .addComponent(b_gettingTemplate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(154, 154, 154))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, w_gettingTemplateLayout.createSequentialGroup()
                .addGroup(w_gettingTemplateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(w_gettingTemplateLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(l_gettingTemplate, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE))
                    .addGroup(w_gettingTemplateLayout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(pb_gettingTemplate, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)))
                .addGap(127, 127, 127))
        );
        w_gettingTemplateLayout.setVerticalGroup(
            w_gettingTemplateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(w_gettingTemplateLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(l_gettingTemplate)
                .addGap(33, 33, 33)
                .addComponent(pb_gettingTemplate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addComponent(b_gettingTemplate)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Creator");
        setEnabled(false);
        setMinimumSize(new java.awt.Dimension(850, 650));

        jFileChooser1.setControlButtonsAreShown(false);
        jFileChooser1.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
        jFileChooser1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jFileChooser1.setDragEnabled(true);

        jSlider1.setValue(0);
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createCompoundBorder(null, javax.swing.BorderFactory.createCompoundBorder(null, javax.swing.BorderFactory.createEtchedBorder())));
        jPanel1.setDoubleBuffered(false);
        jPanel1.setMaximumSize(new java.awt.Dimension(1024, 32767));
        jPanel1.setPreferredSize(new java.awt.Dimension(125, 500));
        jPanel1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jPanel1ComponentResized(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1233, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 160, Short.MAX_VALUE)
        );

        pPlayer.setDoubleBuffered(true);
        pPlayer.setMaximumSize(new java.awt.Dimension(0, 0));
        pPlayer.setMinimumSize(new java.awt.Dimension(400, 300));

        semanaGroup.add(tb_lunes);
        tb_lunes.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_lunes.setSelected(true);
        tb_lunes.setText("Lunes");
        tb_lunes.setToolTipText("Pulsa para editar el Lunes");
        tb_lunes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_lunesActionPerformed(evt);
            }
        });

        semanaGroup.add(tb_martes);
        tb_martes.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_martes.setText("Martes");
        tb_martes.setToolTipText("Pulsa para editar el Martes");
        tb_martes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_martesActionPerformed(evt);
            }
        });

        semanaGroup.add(tb_miercoles);
        tb_miercoles.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_miercoles.setText("Miercoles");
        tb_miercoles.setToolTipText("Pulsa para editar el Miércoles");
        tb_miercoles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_miercolesActionPerformed(evt);
            }
        });

        semanaGroup.add(tb_jueves);
        tb_jueves.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_jueves.setText("Jueves");
        tb_jueves.setToolTipText("Pulsa para editar el Jueves");
        tb_jueves.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_juevesActionPerformed(evt);
            }
        });

        semanaGroup.add(tb_viernes);
        tb_viernes.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_viernes.setText("Viernes");
        tb_viernes.setToolTipText("Pulsa para editar el Viernes");
        tb_viernes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_viernesActionPerformed(evt);
            }
        });

        semanaGroup.add(tb_sabado);
        tb_sabado.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_sabado.setText("Sábado");
        tb_sabado.setToolTipText("Pulsa para editar el Sábado");
        tb_sabado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_sabadoActionPerformed(evt);
            }
        });

        semanaGroup.add(tb_domingo);
        tb_domingo.setFont(new java.awt.Font("Verdana", 1, 10)); // NOI18N
        tb_domingo.setText("Domingo");
        tb_domingo.setToolTipText("Pulsa para editar el Domingo");
        tb_domingo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tb_domingoActionPerformed(evt);
            }
        });

        jLabel1.setText("Selecciona el día de la semana a editar:");

        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jSlider2.setMaximum(10);
        jSlider2.setMinimum(1);
        jSlider2.setMinorTickSpacing(1);
        jSlider2.setOrientation(javax.swing.JSlider.VERTICAL);
        jSlider2.setSnapToTicks(true);
        jSlider2.setValue(9);
        jSlider2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider2StateChanged(evt);
            }
        });

        jMenu1.setText("Menu");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        i_nuevo.setText("Nuevo");
        i_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_nuevoActionPerformed(evt);
            }
        });
        jMenu1.add(i_nuevo);

        i_abrir.setText("Abrir");
        i_abrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_abrirActionPerformed(evt);
            }
        });
        jMenu1.add(i_abrir);

        i_guardar.setText("Guardar");
        i_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_guardarActionPerformed(evt);
            }
        });
        jMenu1.add(i_guardar);

        i_guardarcopia.setText("Guardar como");
        i_guardarcopia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_guardarcopiaActionPerformed(evt);
            }
        });
        jMenu1.add(i_guardarcopia);
        jMenu1.add(jSeparator2);

        i_preferences.setText("Preferencias");
        i_preferences.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_preferencesActionPerformed(evt);
            }
        });
        jMenu1.add(i_preferences);
        jMenu1.add(jSeparator1);

        i_salir.setText("Salir");
        i_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_salirActionPerformed(evt);
            }
        });
        jMenu1.add(i_salir);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Conectividad");

        i_send.setText("Mandar datos");
        i_send.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_sendActionPerformed(evt);
            }
        });
        jMenu3.add(i_send);
        i_send.getAccessibleContext().setAccessibleName("Enviar datos");

        i_burn.setText("Grabar en DVD");
        i_burn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_burnActionPerformed(evt);
            }
        });
        jMenu3.add(i_burn);

        i_usb.setText("Grabar en USB");
        i_usb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_usbActionPerformed(evt);
            }
        });
        jMenu3.add(i_usb);

        i_mixed.setText("Crear Playlist");
        i_mixed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_mixedActionPerformed(evt);
            }
        });
        jMenu3.add(i_mixed);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Herramientas");
        jMenu4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu4ActionPerformed(evt);
            }
        });

        i_informacion.setText("Obetener Información");
        i_informacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_informacionActionPerformed(evt);
            }
        });
        jMenu4.add(i_informacion);

        i_changeProxy.setText("Cambiar proxy");
        i_changeProxy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_changeProxyActionPerformed(evt);
            }
        });
        jMenu4.add(i_changeProxy);

        i_horarios.setText("Horarios de las pantallas");
        jMenu4.add(i_horarios);

        i_tvswitch.setText("Encender/apagar pantallas");
        jMenu4.add(i_tvswitch);

        jMenuBar1.add(jMenu4);

        jMenu2.setText("Ayuda");

        jMenuItem6.setText("Acerca de");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSlider1, javax.swing.GroupLayout.DEFAULT_SIZE, 1237, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1237, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jFileChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 837, Short.MAX_VALUE)
                                .addGap(12, 12, 12))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(tb_lunes)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tb_martes)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tb_miercoles)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tb_jueves)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tb_viernes)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tb_sabado)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tb_domingo))
                                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(155, 155, 155)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSlider2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {tb_domingo, tb_jueves, tb_lunes, tb_martes, tb_miercoles, tb_sabado, tb_viernes});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSlider2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jFileChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(tb_lunes)
                                    .addComponent(tb_martes)
                                    .addComponent(tb_miercoles)
                                    .addComponent(tb_jueves)
                                    .addComponent(tb_viernes)
                                    .addComponent(tb_sabado)
                                    .addComponent(tb_domingo)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void i_informacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_informacionActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_i_informacionActionPerformed

    private void i_changeProxyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_changeProxyActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_i_changeProxyActionPerformed

    private void jMenu4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu4ActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_jMenu4ActionPerformed

    private void i_mixedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_mixedActionPerformed
        Mixer mezclador = new Mixer(aread.getColeccion().getClips(),aread.getColeccion().getNumClips(),aread.getColeccion().c_clips,aread.getColeccion().t_clips,aread.getColeccion().getOccupes());
        Clip[] lista = mezclador.doMixer();
        int count = mezclador.index_clips_out;
        try {
            //int index
            tempFile = File.createTempFile("saving",".smil");
        } catch (IOException ex) {
            Fatal(ex.toString());
        }
        String path = tempFile.getAbsolutePath();
        // Delete temp file when program exits.
        tempFile.delete();
        
        SmilWriter smil_writer = new SmilWriter(path,SmilWriter.SAVE);
        smil_writer.newDocument("Media", "Creator","Autor");
        boolean clips = false;
        boolean consecutive= false;
        
        if(count>0){
            Clip iterclip = lista[0];
            clips=true;
            smil_writer.addElement(iterclip,true);
            for(int i=1; i < count;i++){
                iterclip = lista[i];
                smil_writer.addElement(iterclip,true);
            }
        }    
        
        if(!clips){
            Info("No hay clips para guardar");
            
        }
        smil_writer.closeDocument();
        
    }//GEN-LAST:event_i_mixedActionPerformed

    private void b_gettingTemplateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_gettingTemplateActionPerformed
        //System.exit(0);
        w_gettingTemplate.setVisible(false);
        this.setEnabled(true);
        
    }//GEN-LAST:event_b_gettingTemplateActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void i_usbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_usbActionPerformed
        new USBData(this);
    }//GEN-LAST:event_i_usbActionPerformed

    private void b_passwd_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_passwd_cancelActionPerformed
        //this.sd.setCancelar(true);
    }//GEN-LAST:event_b_passwd_cancelActionPerformed

    private void b_passwdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_passwdActionPerformed
        //this.sd.setPasswd(String.valueOf(this.pf_passwd.getPassword()));
    }//GEN-LAST:event_b_passwdActionPerformed

    private void i_burnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_burnActionPerformed
        new BurnData(this);
    }//GEN-LAST:event_i_burnActionPerformed

    private void jSlider2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider2StateChanged
            //zoomChanged(jSlider2.getValue());
            aread.makeZoom(jSlider2.getValue());
        if(jSlider2.getValue()==10){
                jSlider1.setEnabled(false);
                tb_lunes.setEnabled(false);
                tb_martes.setEnabled(false);
                tb_miercoles.setEnabled(false);
                tb_jueves.setEnabled(false);
                tb_viernes.setEnabled(false);
                tb_sabado.setEnabled(false);
                tb_domingo.setEnabled(false);
            }
            else{
                jSlider1.setEnabled(true);
                tb_lunes.setEnabled(true);
                tb_martes.setEnabled(true);
                tb_miercoles.setEnabled(true);
                tb_jueves.setEnabled(true);
                tb_viernes.setEnabled(true);
                tb_sabado.setEnabled(true);
                tb_domingo.setEnabled(true);
            }
    }//GEN-LAST:event_jSlider2StateChanged

    /**
     * 
     * Make the needed changes in the interfaz if the zoom 
     * of drawing area has change.
     * @param newValue the value of new zoom
     */
    public void zoomChanged(int newValue){
        jSlider2.setValue(newValue);
    }
    
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        w_crew.setVisible(true);
        Action updateCursorAction = new AbstractAction() {
            
            boolean shouldDraw = false;
            public void actionPerformed(java.awt.event.ActionEvent e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                w_crew.setVisible(false);
            }
        };

        new Timer(10, updateCursorAction).start();
        //w_crew.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void i_sendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_sendActionPerformed
        new SendData(this);
    }//GEN-LAST:event_i_sendActionPerformed

    private void b_pref_acceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_pref_acceptActionPerformed
        if (provisionallook != actuallook)
            actuallook = provisionallook;
        w_preferences.setVisible(false);
        options_changed = false;
    }//GEN-LAST:event_b_pref_acceptActionPerformed

    private void b_nimbusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_nimbusActionPerformed
        provisionallook = NIMBUSLOOK;
        changeLook(NIMBUSLOOK);
        options_changed = true;
    }//GEN-LAST:event_b_nimbusActionPerformed

    private void b_swingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_swingActionPerformed
        provisionallook = DEFAULTLOOK;
        changeLook(DEFAULTLOOK);
        options_changed = true;
    }//GEN-LAST:event_b_swingActionPerformed

    private void b_defaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_defaultActionPerformed
        provisionallook = NIMRODLOOK;
        changeLook(NIMRODLOOK);
        options_changed = true;
    }//GEN-LAST:event_b_defaultActionPerformed

    private void b_pref_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_pref_cancelActionPerformed
        if (provisionallook != actuallook){
            switch(actuallook){
                case NIMRODLOOK:
                    b_default.setSelected(true);
                    break;
                case NIMBUSLOOK:
                    b_nimbus.setSelected(true);
                    break;
                case DEFAULTLOOK:
                    b_swing.setSelected(true);
                    break;                    
            }
            changeLook(actuallook);
            provisionallook = actuallook;
        }
        w_preferences.setVisible(false);
        options_changed = false;
    }//GEN-LAST:event_b_pref_cancelActionPerformed

    private void i_preferencesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_preferencesActionPerformed
        w_preferences.setVisible(true);
    }//GEN-LAST:event_i_preferencesActionPerformed

    private void f_openfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f_openfileActionPerformed

        if (f_openfile.CANCEL_OPTION!= returnVal){
            if (f_openfile.getSelectedFile() != null)
                 openFile(f_openfile.getSelectedFile());
        }
        else{
            w_openfile.setVisible(false);
        }
                
    }//GEN-LAST:event_f_openfileActionPerformed

    private void i_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_nuevoActionPerformed
        
        aread.restart(pPlayer);
        aread.repaint();
        pPlayer.repaint();
        System.gc();
//        aread = null;
//        aread = new DibujoArea(pPlayer);
//        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
//        jPanel1.setLayout(jPanel1Layout);
//        jPanel1Layout.setHorizontalGroup(
//            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 100, Short.MAX_VALUE)
//            .addComponent(aread, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE)
//        );
//        jPanel1Layout.setVerticalGroup(
//            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 100, Short.MAX_VALUE)
//            .addComponent(aread, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE)
//        );
    }//GEN-LAST:event_i_nuevoActionPerformed

    private void i_guardarcopiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_guardarcopiaActionPerformed
        if (aread.getColeccion().getNumClips()<1)
            Info("No hay clips para guardar");
        else
            returnVal = f_writefile.showSaveDialog(w_writefile);
    }//GEN-LAST:event_i_guardarcopiaActionPerformed

    private void i_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_guardarActionPerformed
        if (aread.getColeccion().getNumClips()<1)
            Info("No hay clips para guardar");
        else{
            if (already_saved){
                if (aread.saveSMIL(save_path)==true)
                    Info("Guardado correctamente");            
                else
                    Fatal("Error guardando");
            }
            else{
                saving = true;
                w_writefile.setVisible(true);
            }
        }
    }//GEN-LAST:event_i_guardarActionPerformed

    private void f_writefileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f_writefileActionPerformed
           w_writefile.setVisible(false);
           if (f_writefile.CANCEL_OPTION != 0)     
           if (f_writefile.getSelectedFile() != null){
                if (saving == true){
                    if (aread.saveSMIL(f_writefile.getSelectedFile().getAbsolutePath())==true){
                        save_path = f_writefile.getSelectedFile().getAbsolutePath();
                        already_saved = true;
                        Info("Guardado correctamente");  
                    }
                    else
                        Fatal("Error guardando");
                }
                else
                    if (aread.saveSMIL(f_writefile.getSelectedFile().getAbsolutePath())==true)
                        Info("Guardado correctamente");
                    else
                        Fatal("Error guardando");
                
           }
    }//GEN-LAST:event_f_writefileActionPerformed

    private void i_abrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_abrirActionPerformed
        //w_openfile.setVisible(true);
        returnVal = f_openfile.showOpenDialog(w_openfile);
        
    }//GEN-LAST:event_i_abrirActionPerformed

    private void i_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_salirActionPerformed
           System.exit(0);
    }//GEN-LAST:event_i_salirActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        
        w_acercade.setVisible(true);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jPanel1ComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jPanel1ComponentResized
        try{
            aread.newMark(jSlider1.getValue());
        } catch(java.lang.NullPointerException ex){}            
    }//GEN-LAST:event_jPanel1ComponentResized

    private void tb_domingoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_domingoActionPerformed
        dayChanged(6);
    }//GEN-LAST:event_tb_domingoActionPerformed

    private void tb_sabadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_sabadoActionPerformed
        dayChanged(5);
    }//GEN-LAST:event_tb_sabadoActionPerformed

    private void tb_viernesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_viernesActionPerformed
        dayChanged(4);
    }//GEN-LAST:event_tb_viernesActionPerformed

    private void tb_juevesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_juevesActionPerformed
        dayChanged(3);
    }//GEN-LAST:event_tb_juevesActionPerformed

    private void tb_miercolesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_miercolesActionPerformed
        dayChanged(2);
    }//GEN-LAST:event_tb_miercolesActionPerformed

    private void tb_martesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_martesActionPerformed
        dayChanged(1);
    }//GEN-LAST:event_tb_martesActionPerformed

    private void tb_lunesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tb_lunesActionPerformed
        dayChanged(0);
    }//GEN-LAST:event_tb_lunesActionPerformed

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        // TODO add your handling code here:
        aread.newMark(jSlider1.getValue());
    }//GEN-LAST:event_jSlider1StateChanged
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gui().setVisible(true);
            }
        });
    }
    
    private void dayChanged(int day){        
        aread.changeDay(day);
        jSlider1.setValue((int)(((ClipCollection.MAX_VALUE_SLIDER1/7.0)*day*1.1)+7.1));
//        Main.logger.info((int)(((ClipCollection.MAX_VALUE_SLIDER1/7.0)*day*1.1)+7.1));
        aread.repaint();
    }
    
    private void openFile(java.io.File fil){
        //morralla
        w_openfile.setVisible(false);
        if (f_openfile.CANCEL_OPTION != 0)     
            if (f_openfile.getSelectedFile() != null){
                setEnabled(false);
                if_importing.setVisible(true);
                repaint();
                SmilParser parser = new SmilParser(fil.getAbsolutePath(),false);
                SimpleClip[] clip_list = parser.getClipsList();
                //aread.restart(pPlayer);
                aread.repaint();
                pPlayer.repaint();
                aread.restart(pPlayer);
                //java.io.File fila ;
                ClipCollection coleccion;
                //System.gc();
                //for(int i=0;i<clip_list.length ; i++){
                aread.getColeccion().addListClips(clip_list);
                setEnabled(true);
                if_importing.setVisible(false);              
            }
    }
    
    private void changeLook(int look){
        
        String lookandfeel = "";
        try{
            switch(look){
                case NIMRODLOOK:
                    lookandfeel = "com.nilo.plaf.nimrod.NimRODLookAndFeel";
                    break;
                case NIMBUSLOOK:
                    lookandfeel = "org.jdesktop.swingx.plaf.nimbus.NimbusLookAndFeel";
                    break;
                case DEFAULTLOOK:
                    lookandfeel = "javax.swing.plaf.metal.MetalLookAndFeel";
                    break;                    
            }
            //UIManager.setLookAndFeel("org.jdesktop.swingx.plaf.nimbus.NimbusLookAndFeel");
            UIManager.setLookAndFeel(lookandfeel );
            SwingUtilities.updateComponentTreeUI(this);
            SwingUtilities.updateComponentTreeUI(w_preferences);
            SwingUtilities.updateComponentTreeUI(w_writefile);
            SwingUtilities.updateComponentTreeUI(w_openfile);
            SwingUtilities.updateComponentTreeUI(w_acercade);
            SwingUtilities.updateComponentTreeUI(w_senddata);
            SwingUtilities.updateComponentTreeUI(w_gettingTemplate);
        }
        catch(java.lang.ClassNotFoundException e){
            System.out.println(e);
        }
        catch(java.lang.InstantiationException  e){
            System.out.println(e);
        }
        catch(java.lang.IllegalAccessException  e){
            System.out.println(e);
        }
        catch(javax.swing.UnsupportedLookAndFeelException e){
            System.out.println(e);
        }
    }
    
        
    /**
     * Show a message, generally an error. Need com.sun.media.ui.*
     *
     * @param s         The message to show.
     */
    static void Fatal(String s) {
        MsgBox mb = new MsgBox(new Frame(),s,false);
       
        
    }
    /**
     * Show a info message. Need com.sun.media.ui.*
     *
     * @param s         The message to show.
     */
    static void Info(String s) {
        InfoBox mb = new InfoBox(new Frame(),s,false);
       
    }
    
    public void updateTemplate(boolean err){
        pb_gettingTemplate.setIndeterminate(false);
        pb_gettingTemplate.setValue(100);
        if (err == true){
            l_gettingTemplate.setText("Sin conexión con la Junta, necesita internet para comenzar");
            pb_gettingTemplate.setVisible(false);
            b_gettingTemplate.setVisible(true);
            //System.exit(0);
        }
        else{
            l_gettingTemplate.setText("Plantilla de contenidos recibida");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            w_gettingTemplate.setVisible(false);
            this.setEnabled(true);
            aread.repaint();
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_cancel_send;
    private javax.swing.JRadioButton b_default;
    private javax.swing.JButton b_gettingTemplate;
    private javax.swing.JRadioButton b_nimbus;
    private javax.swing.JButton b_passwd;
    private javax.swing.JButton b_passwd_cancel;
    private javax.swing.JButton b_pref_accept;
    private javax.swing.JButton b_pref_cancel;
    private javax.swing.JRadioButton b_swing;
    private javax.swing.JFileChooser f_openfile;
    private javax.swing.JFileChooser f_writefile;
    private javax.swing.JMenuItem i_abrir;
    private javax.swing.JMenuItem i_burn;
    private javax.swing.JMenuItem i_changeProxy;
    private javax.swing.JMenuItem i_guardar;
    private javax.swing.JMenuItem i_guardarcopia;
    private javax.swing.JMenuItem i_horarios;
    private javax.swing.JMenuItem i_informacion;
    private javax.swing.JMenuItem i_mixed;
    private javax.swing.JMenuItem i_nuevo;
    private javax.swing.JMenuItem i_preferences;
    private javax.swing.JMenuItem i_salir;
    private javax.swing.JMenuItem i_send;
    private javax.swing.JMenuItem i_tvswitch;
    private javax.swing.JMenuItem i_usb;
    private javax.swing.JInternalFrame if_importing;
    private javax.swing.JButton jButton1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel_sending_iso;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JSlider jSlider2;
    private javax.swing.JLabel l_gettingTemplate;
    private javax.swing.JLabel l_iconsending;
    private javax.swing.JLabel l_passwd;
    private javax.swing.JLabel l_senddata;
    private javax.swing.ButtonGroup lookGroup;
    private javax.swing.JDesktopPane pPlayer;
    private javax.swing.JProgressBar p_sending;
    private javax.swing.JProgressBar pb_gettingTemplate;
    private javax.swing.JPasswordField pf_passwd;
    private javax.swing.ButtonGroup semanaGroup;
    private javax.swing.JPanel t_apparence;
    private javax.swing.JPanel t_options;
    private javax.swing.JTabbedPane t_padre;
    private javax.swing.JToggleButton tb_domingo;
    private javax.swing.JToggleButton tb_jueves;
    private javax.swing.JToggleButton tb_lunes;
    private javax.swing.JToggleButton tb_martes;
    private javax.swing.JToggleButton tb_miercoles;
    private javax.swing.JToggleButton tb_sabado;
    private javax.swing.JToggleButton tb_viernes;
    private javax.swing.JFrame w_acercade;
    private javax.swing.JDialog w_crew;
    private javax.swing.JDialog w_gettingTemplate;
    private javax.swing.JFrame w_login;
    private javax.swing.JDialog w_openfile;
    private javax.swing.JDialog w_preferences;
    private javax.swing.JFrame w_senddata;
    private javax.swing.JDialog w_writefile;
    // End of variables declaration//GEN-END:variables
    private DibujoArea aread;
    private String[] semana;
    private JToggleButton[] semana_buttons;
    private int actual_day;
    private final int NIMBUSLOOK = 1;
    private final int NIMRODLOOK = 2;
    private final int DEFAULTLOOK = 3;
    private int actuallook = NIMRODLOOK;
    private int provisionallook = NIMRODLOOK;
    private boolean options_changed = false;
    private boolean already_saved = false;
    private String save_path;
    private boolean saving = false;
    private int returnVal = 0;
    private File tempFile;
    
    public Color c_lunes;
    public Color c_martes;
    public Color c_miercoles;
    public Color c_jueves;
    public Color c_viernes;
    public Color c_sabado;
    public Color c_domingo;

    public javax.swing.JFrame getW_senddata() {
        return w_senddata;
    }

    public void setW_senddata(javax.swing.JFrame w_senddata) {
        this.w_senddata = w_senddata;
    }

    public DibujoArea getAread() {
        return aread;
    }

    public void setAread(DibujoArea aread) {
        this.aread = aread;
    }

    public javax.swing.JPanel getJPanel_sending_iso() {
        return jPanel_sending_iso;
    }

    public javax.swing.JFrame getW_login() {
        return w_login;
    }

    public void setW_login(javax.swing.JFrame w_login) {
        this.w_login = w_login;
    }

    public javax.swing.JPasswordField getPf_passwd() {
        return pf_passwd;
    }

    public void setPf_passwd(javax.swing.JPasswordField pf_passwd) {
        this.pf_passwd = pf_passwd;
    }

    public javax.swing.JMenu getJMenu3() {
        return jMenu3;
    }
}

class SmilFilter extends javax.swing.filechooser.FileFilter {
    public boolean accept(java.io.File f) {
        return f.isDirectory() || f.getName().toLowerCase().endsWith(".smil");
    }
    
    public String getDescription() {
        return "SMIL files";
    }

}
