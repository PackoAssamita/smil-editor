/*
 * TemplateManager.java
 *
 * Created on 22 de octubre de 2007, 14:04
 * 
(C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.assamita.smileditor.editor;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.File.*;

/**
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */
public class TemplateManager implements Runnable{
    
    public TemplateManager(ClipCollection padre){
        parent = padre;
//        clips_mask = t_clips;
//        clips_content = c_clips;
    }
    
    /** Creates a new instance of TemplateManager */
    public void run (){
        getFileUrl(CONTENT);
        getFileUrl(TEMPLATE);
        SmilParser smilParser = new SmilParser(path_mask,true);
        if (smilParser.ERRORS == false){
            clips_mask = smilParser.getClipsList();
            parent.continueWithTemplate();
        }
        else{
            ERRORS = true;
            ERROR = "Error parsing file";
            parent.continueWithTemplate();
        }
        smilParser = new SmilParser(path_content,true);
        if (smilParser.ERRORS == false){
            clips_content = smilParser.getClipsList();
            parent.continueWithTemplate();
        }
        else{
            ERRORS = true;
            ERROR = "Error parsing file";
            parent.continueWithTemplate();
        }
    }
    
    public SimpleClip[] getMaskClipList(){
        //SimpleClip[] clips;
        
        return  clips_mask;
    }
    
    public SimpleClip[] getContentClipList(){
        //SimpleClip[] clips;
        
        return  clips_content;
    }
        
    private void getFileUrl(int action){
        try {
            tempFile = File.createTempFile("template",".smil");
            // Delete temp file when program exits.
            tempFile.deleteOnExit();
            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));
            
            if(action == CONTENT)
                url = new URL(url_path_content);
            else
                url = new URL(url_path_mask);
            //System.out.println(url);
            InputStream in;
            in = url.openStream();
            
            // Create a buffered input stream for efficency
            BufferedInputStream bufIn = new BufferedInputStream(in);

            // Repeat until end of file
            for (;;)
            {
                int data = bufIn.read();

                // Check for EOF
                if (data == -1)
                    break;
                else
                    out.write(data);
            }
            bufIn.close();
            out.close();
            in.close();            
        } catch (MalformedURLException ex) {
            ERROR = ex.toString();
            ERRORS = true;
        } catch (IOException ex) {
            ERROR = ex.toString();
            ERRORS = true;
        }
        if (action == CONTENT)
            path_content = tempFile.getAbsolutePath();
        else
            path_mask    = tempFile.getAbsolutePath();
    }
    
    private URL url;
    private String url_path_mask = "http://localhost/template_playlist.smil";
    private String url_path_content = "http://localhost/template_playlist_content.smil";
    public String ERROR = "";
    public boolean ERRORS = false;
    private File tempFile;
    private ClipCollection parent;
    private SimpleClip[] clips_mask;
    private SimpleClip[] clips_content;
    private int TEMPLATE = 0;
    private int CONTENT  = 1;
    public String path_content;
    public String path_mask;
    
}
