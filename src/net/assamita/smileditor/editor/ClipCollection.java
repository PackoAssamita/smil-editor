/**
 * clipcollection.java
 *
 * Created on 21 de febrero de 2007, 11:54
 *
 * Manage a collection of clips and draw the passed graphics context
 * representing the clips.
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.


 */

package net.assamita.smileditor.editor;

import com.ibm.media.codec.audio.g723.G723Dec;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import javax.imageio.ImageReader;
import javax.media.*;
import com.sun.media.ui.*;
import javax.media.protocol.*;
import javax.media.protocol.DataSource;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.Vector;


/**
 * Manage a collection of clips and draw the passed graphics context
 * representing the clips.
 * @author      Francisco Jose Moreno Llorca
 */
public class ClipCollection {
    
    /**
     * Creates a new instance of ClipCollection
     */
    public ClipCollection(JDesktopPane player,Gui padre) throws InterruptedException, InvocationTargetException{
        //desciption of variables at the end of file
        //day = given_day;
        dPlayer = player;
        parent = padre;
        skin = new Skin();
        clips   = new Clip[MAX_NUM_CLIPS];
        occuped = new int[DAY_MAX_TIME];
        ocupados = 0;
        tomorrow = null;
        free    = new boolean[MAX_NUM_CLIPS];
        verde_corporativo = new Color(0,114,91);
        yellow_negative   = new Color(228,193,88);
        black_green       = new Color(33,85,75);
        reserved          = new Color(91,181,203);
        dPlayer.setBackground(verde_corporativo);
        semana = new String[] {"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
        playing = false;
        isClipSelected = false;
        zoom = 9;
        steptime = new Multitime();
        int i=0;
        for (i=0;i<DAY_MAX_TIME;i++)
            occuped[i]=-1;
        for (i=0;i<MAX_NUM_CLIPS;i++)
            free[i]=true;
        
        //get template
        template_manager = new TemplateManager(this);
//        SwingUtilities.invokeLater(template_manager);
        Thread thread = new Thread (template_manager);
        thread.setDaemon(true);
        thread.start();
        
//        if (template_manager.ERRORS == true){
//            Fatal("Sin conexión con el proveedor, necesita internet para comenzar");
//            System.exit(0);
//        }
//        t_clips = template_manager.getClipList();
        t_clips = new SimpleClip[0];
    }
    
    public void continueWithTemplate(){
        boolean err;
        if (template_manager.ERRORS == true){
            //Fatal("Sin conexión con el proveedor, necesita internet para comenzar");
            err = true;
            //System.exit(0);
        }
        else{
            t_clips = template_manager.getMaskClipList();
            c_clips = template_manager.getContentClipList();
            err = false;
        }
        parent.updateTemplate(err);
        
        
    }
    
    /**
     * Show a message, generally an error. Need com.sun.media.ui.*
     *
     * @param s         The message to show.
     */
    static void Fatal(String s) {
        MsgBox mb = new MsgBox(new Frame(),s,false);
        isClipSelected = false;
    } 
    
    /**
     * add a Clip to collection
     * 
     * 
     * @param fichero   The media file added to collection
     * @param width     The width of parent widget
     * @param loc       The X coordinate where the file is added in the widget
     * @param marca     The value of the offset of time line across the parent widget
     */
    public void add(String fichero,int width,int loc,int marca){
        //preview of Clip
        if(isSupported(fichero)){
            if(playing ){
                clipjmf.stopMedia();
                dPlayer.remove(clipjmf);
            }

            clipjmf_aux = new PlayerJMF("file:"+fichero);
            Multitime mtime = new Multitime(clipjmf_aux.getDuration());
            if (clipjmf_aux.getDuration()!=0 && mtime.getPixels(DAY_MAX_TIME) > this.REDIMENSION){
            //if (clipjmf_aux.getDuration()!=0 ){
    //        if (true){
                clipjmf = clipjmf_aux;
                int first = firstFree();
                Multitime time = new Multitime(clipjmf.getDuration());
                //Multitime time = new Multitime();
                //time.setPixels()
                //Main.logger.info("factorSlide(loc)");
                //Main.logger.info(factorSlide(loc));
                //Main.logger.info(loc);
                //Main.logger.info(factorSlide());
                int firstPlace = firstFreePlace(factorSlide(loc),time);
                if(firstPlace > 0){

                    clips[first] = new Clip(fichero,first,0,clipjmf.getDuration());

                    clips[first].setStart(firstPlace);
                    int i=0;
                    for(i=clips[first].getStart(); i < clips[first].getEnd();i++)
                        occuped[i] = first;
                    free[first] = false;
                    ocupados++;
                    //this.clipDeselect();
                    dPlayer.add(clipjmf);

                    try{
                        clipjmf.setMaximum(true);
                        clipjmf.setIconifiable(false);
                        clipjmf.setMaximizable(false);
                        clipjmf.setClosable(false);
                    }
                    catch (java.beans.PropertyVetoException e){
                        Fatal(e.toString());
                    }

                    dPlayer.setVisible(true);
                    playing = true;
                }
                else{
                    clipjmf_aux.stopMedia();
                    Fatal("No queda un espacio libre para el medio. Prueba a insertarlo antes o en otro día");
                }
            }
            else if (mtime.getPixels(DAY_MAX_TIME) <= this.REDIMENSION){
                String sweek ="Video demasiado corto para esta vista,prueba haciendo zoom";
                String sday = "Video demasiado corto para esta vista";
                String msg;
                if(zoom == DAY)
                    msg = sday;
                else
                    msg = sweek;
                Fatal(msg);
            }
        }
    }
    
     /**
     * add a Clip to collection
     * 
     * 
     * @param clips   the list of clips
     */
    public void addListClips(SimpleClip[] clips_ext){
        //preview of Clip
        String fichero;
        int first,start;
        SimpleClip aclip;  //for use in for "actualclip" -> aclip
        int duration=0;
        for(int k=0; k < clips_ext.length; k++){
            aclip = clips_ext[k];
            fichero = aclip.url.toString();
            if(isSupported(fichero)){
                if(playing ){
                    clipjmf.stopMedia();
                    dPlayer.remove(clipjmf);
                }
                
                clipjmf_aux = new PlayerJMF(fichero);
//                if (aclip.numrep>0){
//                    if (aclip.lastoffset != -1)
//                        duration = clipjmf_aux.getDuration()*( aclip.numrep-1 ) + aclip.lastoffset;
//                    else
//                        duration = clipjmf_aux.getDuration()* aclip.numrep;
//                }
                
                //Multitime time = new Multitime(duration);
                duration = clipjmf_aux.getDuration();
                aclip.recalculate(duration);              
                clipjmf_aux.stopMedia();
                if (clipjmf_aux.getDuration()!=0 ){
        //        if (true){
                    //clipjmf = clipjmf_aux;
                    first = firstFree();    
                    if (aclip.begin == -1 ){
                        if( first >0){
                            start = clips[first-1].getEnd();
                        }
                        else
                            start = 0;
                        //clips[first].setStart(clips[first-1].getEnd());
                    }
                    else
                        start = aclip.getStart();
                    clips[first] = new Clip(aclip.src,aclip.id,start,duration);
                    //Width
//                    if (clips[first].getWidth()==-1){
//                        if (clips[first].isRedStart())
//                            clips[first].setWidth(time.getPixels(DAY_MAX_TIME)-(clips[first].getStart()-clips[first].getRedStart()));
//                        else
//                            clips[first].setWidth(time.getPixels(DAY_MAX_TIME));
//                    }
                    //start
                   if (aclip.startClip != 0){
                        clips[first].setRedStartOpening();
                   }
                   if (aclip.numRep > 0){
                        clips[first].setRedWidthOpening(aclip.duration);
                   }

                    //Durationi
                    //clips[first].setDuration(duration);
                    //stopClip();
                    int i=0;
                    for(i=clips[first].getStart(); i < clips[first].getWidth()+clips[first].getStart();i++){
                        occuped[i] = first; 
                    }
                    free[first] = false;
                    ocupados++;
                }
            }
        }
    }
    
    
    /**
     * calculate the first place in timeline where a Clip fit with duration dur
     * and begining from loc to the rigth 
     * 
     * 
     * @param loc the position along the timeline where the Clip may insert
     * @param time the duration of the Clip
     * @return the start position of Clip or -1 if there is'nt any place
     */
    private int firstFreePlace(int loc, Multitime time){
        int i = 0;
        int j = 0;
        boolean occ = false;
        //max_time ya no es tiempo, no se corresponde con dur
        try{
            for(i=loc; i < DAY_MAX_TIME- time.getPixels(DAY_MAX_TIME) ; i++){
                if (occuped[i] == -1 ){
                    for (j=i; j < i+time.getPixels(DAY_MAX_TIME) && j< DAY_MAX_TIME - time.getPixels(DAY_MAX_TIME); j++ ){
                        if (occuped[j] != -1 )
                            occ = true;
                    }
                    if (!occ)
                        return i;
                    else
                        occ = false;
                }
        }
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal(e.toString());
                return -1;
            }
        return -1;
    }
    
    /**
     * return the first free position in clips, if there isnt any, return -1
     *
     * @return the first free position in clips, if there isnt any, return -1
     */
    private int firstFree(){
        int i=0,result=0;
        for(i=0;i< MAX_NUM_CLIPS ; i++){
            if (free[i] == true)
                return i;
        }
        return -1;
    }
    
    /**
     * get if the Clip is supported or not, only check the extension
     * 
     * 
     * @return true if the Clip is supported or not in other case
     */
    private boolean isSupported(String filename){        
        int i = filename.lastIndexOf('.');
        String extension = filename.substring(i+1);
        for(i=0;i<supportedFormats.length;i++){
            if(extension.compareTo(supportedFormats[i]) == 0 )
                return true;
        }
        Fatal("El archivo seleccionado parece no ser de video");
        return false;
    }
    
    /**
     * return the Clip drawed around the (@link loc) coordinate or a new Clip
     * if there is any Clip.
     * 
     * 
     * @param width     The width of parent widget
     * @param loc       The X coordinate where the file is added in the widget
     * @param marca     The value of the offset of time line across the parent widget
     * @return the Clip drawed around the (@link loc) coordinate or a new Clip
     * if there is any Clip.
     */
    public Clip getClipByLocation(int loc){
        /*int i = 0, start;
        for(i=0;i < ocupados; i++){
            if ((factorSlide(marca,width)+loc) < (clips[i].getStart()+clips[i].getWidth()) && (factorSlide(marca,width)+loc) > (clips[i].getStart()) )
                return clips[i];
        }
         */
        try{
            if (occuped[factorSlide(loc)] != -1)
                return clips[occuped[factorSlide(loc)]];
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal(e.toString());
                return null;
            }
        return null;
    }
    
    public Clip getClipByLocationTotal(int loc_total){
        /*int i = 0, start;
        for(i=0;i < ocupados; i++){
            if ((factorSlide(marca,width)+loc) < (clips[i].getStart()+clips[i].getWidth()) && (factorSlide(marca,width)+loc) > (clips[i].getStart()) )
                return clips[i];
        }
         */
        try{
            
            if (occuped[loc_total] != -1)
                return clips[occuped[loc_total]];
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal("getClipByLocationTotal: "+e.toString());
                return null;
            }
        return null;
    }
    
    /**
     * get if there is a Clip in the region
     * 
     * 
     * @param init     the begining of region
     * @param end       the end of region
     * @return true if there is a Clip at least in the region, false in other
     * case
     */
    public boolean isClipInRegion(int init, int end, Clip clipy){
        int i = 0;
        int id = clipy.getID();
        try{
        for(i=init;i < end; i++){
            if (occuped[i] != -1 && occuped[i] != id)
                return true;
        }
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal("isClipInRegion: "+e.toString());
                return true;
            }
        return false;
    }
    
    public boolean isClipInRegionSplit(int width){
        int i = 0;
        try{
        for(i=0;i < width; i++){
            if (occuped[i] != -1)
                return true;
        }
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal("isClipInRegion: "+e.toString());
                return true;
            }
        return false;
    }
    
    /**
     * return true if there is a Clip around the loc
     * 
     * 
     * @param loc       The X coordinate where the file is added in the widget
     * @return true if there is a Clip around the loc, false in other case.
     */
    public boolean isClipByLocation(int loc){
//        int i = 0, start;
//        for(i=0;i < ocupados; i++){
//            if ((factorSlide(marca,width)+loc) < (clips[i].getStart()+clips[i].getWidth()) && (factorSlide(marca,width)+loc) > (clips[i].getStart()) )
//                return true;
//        }
//        return false;
        try{
        //if (occuped[factorSlide(loc)] != -1)
        if (occuped[factorSlide(loc)] != -1)
            return true;
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal("isClipByLocation:  "+ e.toString());
                return true;
            }
        return false;
    }
    
    /**
     * return true if there is a Clip around the loc
     * 
     * 
     * @param loc       The coordenate respect the widget where the file is added in the widget
     * @param clipy     the ghost Clip, it space is no consider
     * @return true if there is a Clip around the loc, false in other case.
     */
    public boolean isClipByLocation(int loc, Clip clipy){
        int id = clipy.getID();
        try{
        if (occuped[loc] != -1 && occuped[loc]!=id )
            return true;
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
                Fatal("isClipByLocation2: "+e.toString());
                return true;
            }
        return false;
    }
    
    /**
     * change the location of the Clip in a move process before release mouse
     * 
     * 
     * @param moving Clip to move
     * @param loc       new position
     */
    public void moveClip(Clip moving,int loc){
//        if(moving.isSplited()){
//            if (moving.getSplitedFrom() == -1){
//                
//            }
//        }
//        if (zoom==DAY)
            moving.setMoveStart(loc);
//        else
//            moving.setMoveStart((int)(loc*1.009));
    }
    
    /*
     * make the necesary changes to split a Clip at the end of a day and the
     * beginning of the next day
     *
     * @param clipy the Clip to split
     */
//    private boolean splitClip(Clip clipy){
//        Clip next_day;
//        if(clipy.isSplited()){
//            clipy.setSplit(false);
//            //next_day = tomorrow.getClipByLocationTotal(1);
//            tomorrow.deleteClip(tomorrow.getClipByLocationTotal(1));
//        }
//        //else{
//            clipy.setSplit(max_time - clipy.getMoveStart());
//            next_day = new Clip(clipy);
//            next_day.setSplitedFrom(clipy.getID());
//            next_day.setStart(0);
//            next_day.setSplitWidth(clipy.getSplitWidth()-clipy.getWidth());
//
//            if ( !tomorrow.addSplited(next_day)){
//                clipy.setSplit(false);
//                return false;
//            }
//            else
//                return true;
//        //}
//    }
//    
//    private void splitClipDel(Clip clipy){
//        clipy.setSplit(false);
//        tomorrow.deleteClip(tomorrow.getClipByLocationTotal(1));
//    }
//    
//    public boolean addSplited(Clip clipy){
//        if (!isClipInRegionSplit(clipy.getWidth())){
//            int first = firstFree();
//            clips[first] = clipy;
//            int i=0;
//            for(i=clips[first].getStart(); i < clips[first].getWidth();i++)
//                occuped[i] = first;
//            free[first] = false;
//            ocupados++;
//            return true;
//        }
//        else
//            return false;
//    }
    
    /**
     * a Clip is moved, now has to determine the final position
     * of the Clip, avoiding collisions
     * 
     * 
     * @param fichero   The media file added to collection
     */
    //public void releaseMovement(Clip clipmv,java.awt.event.MouseEvent e){
    public void releaseMovement(){
        // si hay algún Clip en la posicion requerida:
        //for(int j = clipSelected.getMoveStart(); j < clipSelected.getWidth();j++)
        //       if (!isClipByLocation(j))
//        if (clipSelected.getMoveStart()+clipSelected.getWidth()+sticky_umbral >= max_time){
//            clipSelected.setMoveStart(max_time -clipSelected.getWidth()) ;
//        }
        
        if (action == MOVING){
            //put Clip at begining of time line when the Clip cross the left wall
            if (clipSelected.getMoveStart() < 0 && isClipInRegion(0,clipSelected.getWidth(), clipSelected)==false){
                clipSelected.setMoveStart(0);
                occupe(clipSelected);
            }
//            //split the Clip between two days
//            else if ((clipSelected.getMoveStart()+clipSelected.getWidth() > max_time && clipSelected.getMoveStart() < max_time  
//                    && isClipInRegion(max_time - clipSelected.getWidth(),max_time, clipSelected)==false && day!=6) || (clipSelected.getMoveStart()+clipSelected.getSplitWidth() > max_time && clipSelected.getMoveStart() < max_time  
//                    && isClipInRegion(max_time - clipSelected.getSplitWidth(),max_time, clipSelected)==false && day!=6)  ){
//
//
//                if (splitClip(clipSelected))
//                    occupe(clipSelected);
//                else
//                    Fatal("No se puede colocar el medio en esta franja horaria");
//
//            }
            //sticky movement at sunday
            //else if (clipSelected.getMoveStart()+clipSelected.getWidth() + sticky_umbral >= max_time  && isClipInRegion(max_time - clipSelected.getWidth(),max_time, clipSelected)==false){
            else if (clipSelected.getMoveStart()+clipSelected.getWidth() + sticky_umbral >= DAY_MAX_TIME  && isClipInRegion(DAY_MAX_TIME - clipSelected.getWidth(),DAY_MAX_TIME, clipSelected)==false){
                clipSelected.setMoveStart(DAY_MAX_TIME - clipSelected.getWidth());
                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);

            }
            //sticky movement the sunday night
//            else if (clipSelected.getMoveStart()+clipSelected.getWidth() > DAY_MAX_TIME  && isClipInRegion(DAY_MAX_TIME - clipSelected.getWidth(),DAY_MAX_TIME, clipSelected)==false && day==6){
//                clipSelected.setMoveStart(DAY_MAX_TIME - clipSelected.getWidth());
//                occupe(clipSelected);
////                if (clipSelected.isSplited())
////                    splitClipDel(clipSelected);
//
//            }
            //sticky movement to begin of time line
            else if (clipSelected.getMoveStart()-sticky_umbral < 0 && isClipInRegion(clipSelected.getMoveStart(),clipSelected.getMoveStart()+clipSelected.getWidth(), clipSelected)==false){
                clipSelected.setMoveStart(0);
                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);

            }
            //sticky movement to left
            else if (isClipByLocation(clipSelected.getMoveStart()-sticky_umbral,clipSelected) && isClipInRegion(clipSelected.getMoveStart(),clipSelected.getMoveStart()+clipSelected.getWidth(), clipSelected)==false){
                Clip clipy = getClipByLocationTotal(clipSelected.getMoveStart()-sticky_umbral+1);
                clipSelected.setMoveStart(clipy.getStart()+clipy.getWidth());
                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);

            }
            //sticky movement to right
            else if (isClipByLocation(clipSelected.getMoveStart()+clipSelected.getWidth()+sticky_umbral,clipSelected) && isClipInRegion(clipSelected.getMoveStart(),clipSelected.getMoveStart()+clipSelected.getWidth(), clipSelected)==false){
                Clip clipy = getClipByLocationTotal(clipSelected.getMoveStart()+clipSelected.getWidth()+sticky_umbral);
                clipSelected.setMoveStart(clipy.getStart()-clipSelected.getWidth());
                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);

            }
            //sticky movement when put a Clip over other Clip at left
            else if (isClipByLocation(clipSelected.getMoveStart(),clipSelected) && isClipInRegion(getClipByLocationTotal(clipSelected.getMoveStart()).getStart()+getClipByLocationTotal(clipSelected.getMoveStart()).getWidth(),getClipByLocationTotal(clipSelected.getMoveStart()).getStart()+getClipByLocationTotal(clipSelected.getMoveStart()).getWidth()+clipSelected.getWidth(), clipSelected)==false){
                Clip clipy = getClipByLocationTotal(clipSelected.getMoveStart());
                clipSelected.setMoveStart(clipy.getStart()+clipy.getWidth());
                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);

            }
            //sticky movement when put a Clip over other Clip at rigth
            else if (isClipByLocation(clipSelected.getMoveStart()+clipSelected.getWidth(),clipSelected) && isClipInRegion(getClipByLocationTotal(clipSelected.getMoveStart()+clipSelected.getWidth()).getStart()-clipSelected.getWidth(),getClipByLocationTotal(clipSelected.getMoveStart()+clipSelected.getWidth()).getStart(), clipSelected)==false){
                Clip clipy = getClipByLocationTotal(clipSelected.getMoveStart()+clipSelected.getWidth());
                clipSelected.setMoveStart(clipy.getStart()-clipSelected.getWidth());
                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);

            }
            //normal movement
            else if (isClipInRegion(clipSelected.getMoveStart(),clipSelected.getMoveStart()+clipSelected.getWidth(), clipSelected)==false){
                //  arreglar   if(clipSelected.getMoveStart()<0 || clipSelected.getMoveStart()+clipSelected.getWidth()){
                clipSelected.setMoveStart(clipSelected.getMoveStart());    
               // }

                occupe(clipSelected);
//                if (clipSelected.isSplited())
//                    splitClipDel(clipSelected);
            }
            else{
                clipSelected.setMoveStart(clipSelected.getStart());
                this.clipDeselect();
            }
            //this.clipDeselect();
        }
        else if (action == IZRED || action == DERED){
            if(isClipInRegion(clipSelected.getStart(),clipSelected.getStart()+clipSelected.getWidth(), clipSelected)==false){
                clipSelected.setMoveStart(clipSelected.getStart());
                occupe(clipSelected);
            }
            else{
                Fatal("No hay sitio para colocar el clip ahí,prueba moviendolo antes.");
                if (action == IZRED)
                    clipSelected.delRedStartMoving();                
                else
                    clipSelected.delRedWidthMoving();                
            }    
        }
//        else if (action == DERED){
//            if(isClipInRegion(clipSelected.getStart()+clipSelected.getWidth(),clipSelected.getStart()+clipSelected.getWidth(), clipSelected)==false){
//                occupe(clipSelected);
//            }
//            else{
//                Fatal("No hay sitio para colocar el Clip ahí,prueba moviendolo antes.");
//                clipSelected.delRedStart();                
//            }    
//        }
    }
    
    /**
     * mark in the occuped array, the new position respect getMoveStart() result , and free the old positions
     * 
     * 
     * @param clipy the Clip to make modifications
     */
    
    private void occupe (Clip clipy){
        //Main.logger.info("OCCupe: "+Integer.toString(clipy.getStart()));
        int i=0;
//        if (clipy.isSplited()){
//            //no bonito
//            int maximo=clipy.getStart()+clipy.getWidth();
//            if (maximo > max_time)
//                maximo = max_time;
//            for(i=clipy.getStart();i<maximo;i++){
//                
//                occuped[i]=-1;
//            }
//        }
//        else
        {
            if (clipy.isRedStart() && action==IZRED){
                for(i=clipy.getRedStart();i<clipy.getRedStart()+clipy.getRedStartWidth();i++){
                    occuped[i]=-1;
                }
            }
            else if (action==DERED){
                //Main.logger.info("OCCuoi"+Integer.toString(clipy.getRedWidth()));
                for(i=clipy.getStart();i<clipy.getStart()+clipy.getRedWidth();i++)
                    occuped[i]=-1;
            }
            else{
                for(i=clipy.getStart();i<clipy.getStart()+clipy.getWidth();i++)
                    occuped[i]=-1;
                if (action!=DELRED)
                    clipy.setStart(clipy.getMoveStart());
                else
                    clipy.setMoveStart(clipy.getStart());
            }
        }
        for(i=clipy.getStart();i<clipy.getStart()+clipy.getWidth();i++)
            occuped[i]=clipy.getID();
    }
    
    private void desoccupeRed(Clip clipy){
        int i=0;
        for(i=clipy.getStart();i<clipy.getStart()+clipy.getWidth();i++){
                    occuped[i]=-1;
                }
        //for(i=clipy.getStart();i<clipy.getStart()+clipy.getWidth();i++)
        //    occuped[i]=clipy.getID();
    }
    
    /**
     * return max_time, the width of drawable area in pixels
     *
     * @return max_time, the width of drawable area in pixels
     */
    public int getMaxTime(){
        return DAY_MAX_TIME;
    }
    
    /**
     * delete a Clip from the list. This Clip is the defined around the params
     * 
     * 
     * @param loc       The X coordinate where the file is added in the widget
     */
    public void deleteClip(int loc){
        Clip clipy = getClipByLocation(loc);
        deleteClip(clipy);
        
    }
    
    /**
     * delete a Clip from the list.
     * 
     * 
     * @param Clip       the Clip to delete
     */
    public void deleteClip(Clip clipy){
        int i;
        for(i=0;i < ocupados; i++){
            if (clipy.getID() == clips[i].getID() ){
                ocupados--;
                free[i]=true;
                break;
            }
        }
        for (i=i; i < ocupados ; i++)
            clips[i] = clips[i+1];
        for (i=clipy.getStart(); i < clipy.getStart()+ clipy.getWidth() ; i++){
            occuped[i]=-1;
        }
        clipjmf.stopMedia();
        dPlayer.remove(clipjmf);
        dPlayer.repaint();
        playing = false;
        //isClipSelected = false;
        clipDeselect();
    }
    
    /**
     * play the Clip defined around the params
     * 
     * 
     * @param width     The width of parent widget
     * @param loc       The X coordinate where the file is added in the widget
     * @param marca     The value of the offset of time line across the parent widget
     */
    public void playClip(int loc){
        String url="";
        //if (isClipByLocation(factorSlide(loc))){
        if (isClipByLocation(loc)){
            url = getClipByLocation(loc).getUrl();
        }
        if(playing ){
            clipjmf.stopMedia();
            dPlayer.remove(clipjmf);
        }
        clipjmf = new PlayerJMF(url);
        dPlayer.add(clipjmf);
        try{
            clipjmf.setMaximum(true);
            clipjmf.setIconifiable(false);
            clipjmf.setMaximizable(false);
            clipjmf.setClosable(false);
        }
        catch (java.beans.PropertyVetoException e){
            Fatal(e.toString());
        }
        dPlayer.setVisible(true);
        playing = true;
    }
    
    public void stopClip(){
        if(playing ){
            clipjmf.stopMedia();
            dPlayer.remove(clipjmf);
        }
    }
    
    /**
     * draw the parent Panel: the rule, the clips and all components.
     *
     * @param g         The graphics context of parent
     * @param width     The width of parent widget
     * @param height       The X coordinate where the file is added in the widget
     * @param marca     The value of the offset of time line across the parent widget
     */
    public void draw(Graphics g,int height){
        int mark = factorSlide();
        int i =0,w,h,altura,j=0,begin=0;
        int start;
        altura = 125;
        String name;
        
        //background
        g.setColor(skin.BACKGROUND);
        g.fillRect(0,0,width,height);
        
        if(zoom != WEEK){
            ratio_horas = (int)((long)max_time/(long)(24*7));
        }
        else{
            ratio_horas = (max_time)/7;
            //RULE:
            //only a black rectangule
            g.setColor(Color.BLACK);
            g.fillRect(0,height-20,width , height-10);
        }
        
        int rulebegin,ruleend;
        rulebegin = 0;
        
        for (j=0;j<7;j++){
            if (zoom != WEEK){
                begin = (((j*24))*ratio_horas)-mark;
                //begin = (((j*24))*ratio_horas);
                g.setColor(skin.getWeekColor(j));
                g.fillRect(begin,height-20,max_time,height-10);
                for (i=0;i<=24;i++){
                    begin = ((i+(j*24))*ratio_horas)-mark;
//                  begin = ((i+(j*24))*ratio_horas);
                    g.setColor(new Color(171,178,111));
                    g.drawLine(begin,height-10,begin,0);
                    int k=0;
                    for (k=0;k < rayas*2; k++)
                        if (k%2 ==0)
                            g.drawLine(begin+ratio_horas/2,k*(altura)/(rayas*2),begin+ratio_horas/2,(k+1)*(altura)/(rayas*2));
                    g.setColor(Color.WHITE);
                    g.drawString(Integer.toString(i)+"h",begin,height-10);
                }
                
                g.setColor(Color.BLACK);
                g.fillRect(begin,height-10,begin+5,0);
            }
            else{
                begin = (j*ratio_horas);
                g.setColor(Color.BLACK);
                //g.fillRect(0,0,width , 20);
                //for (l=0;l < rayas; l++)
                g.drawLine(begin,height-10,begin,0);
                g.setColor(Color.WHITE);
                g.drawString(semana[j],begin+30,height-10);
            }
            
                    
        }
        
        //reserved clips
        for( i=0; i< t_clips.length ;  i++){
           // if (t_clips[i].src.compareTo("reservado") != 0){   
                w = getVirtualWidth(t_clips[i].width);
                name = t_clips[i].src;
                start = getVirtualStart(t_clips[i].start)-mark;
                g.setColor(reserved);
                g.fillRect(start, 0, w, altura+10);
                g.setColor(Color.WHITE);
                g.fillRect(start+15, 0+15, w-30, altura-20);
                g.setColor(reserved);
                g.drawString(SizeString("Espacio reservado",w -25,g,0),start+20,0+65);
          //  }
        }
        
        //clips
        for(i=0;i < ocupados; i++){
            if(clips[i].isVisible()){
                w = getVirtualWidth(clips[i].getWidth());
                name = clips[i].getName();
                start = getVirtualStart(clips[i].getStart())-mark;
                g.setColor(verde_corporativo);
                g.fillRect(start, 0, w, altura);
                g.setColor(Color.WHITE);
                g.fillRect(start+5, 0+5, w-10, altura-10);
                
                if (zoom != WEEK){
                    steptime.setTime((int)clips[i].getDuration() );
                    int steps;
                    try{
                        steps = w / steptime.getPixels(max_time);
                    }catch(java.lang.ArithmeticException e){
                        steps = 0;
                    }
                    
                    int ite;
                    g.setColor(Color.BLACK);
                    for (ite=1;ite <= steps;ite++){
                            begin = ((start + ite*steptime.getPixels(max_time)));
                            //g.setColor(new Color(171,178,111));
                            //g.drawLine(begin,height-10,begin,0);
                            int k=0;
                            for (k=1;k < points*2; k++)
                                if (k%2 ==0){
                                    if (steps > 1)
                                        g.drawLine(begin,k*(altura)/(points*2),begin,k*(altura)/(points*2)+1);
                                }
                            //g.setColor(Color.WHITE);
                            //g.drawString(Integer.toString(i)+"h",begin,height-10);
                    }
                }
                //____________________________________
            //Multitime steptime = new Multitime((int)clipSelected.getDuration());
            //steptime.setTime((int)clipSelected.getDuration());
            //int steps = w / steptime.getPixels(max_time);
//            for (i=1;i <= steps;i++){
//                    begin = ((start + i*steptime.getPixels(max_time)));
//                    //g.setColor(new Color(171,178,111));
//                    //g.drawLine(begin,height-10,begin,0);
//                    int k=0;
//                    for (k=1;k < points*2; k++)
//                        if (k%2 ==0)
//                            g.drawLine(begin,k*(altura)/(points*2),begin,k*(altura)/(points*2)+1);
//                    //g.setColor(Color.WHITE);
//                    //g.drawString(Integer.toString(i)+"h",begin,height-10);
//                }
            //____________________________________
                g.setColor(verde_corporativo);
                String[] ruta = clips[i].getName().split("/");
                String sname = ruta[ruta.length-1];
//                int width_string = ruta.length*10;
//
//                if (clips[i].getWidth() > width_string){
                g.drawString(SizeString("Medio: ",w,g,0),start+6,0+22);
                g.setColor(Color.BLACK);
                g.drawString(SizeString(sname,w,g,6),start+6+6,0+35);
                g.setColor(verde_corporativo);
                g.drawString(SizeString("Duracion: ",w,g,0),start+6,0+50);
                g.setColor(Color.BLACK);
                g.drawString(SizeString(getStringFromDuration(clips[i].getDuration()),w,g,60),start+6+60,0+50);
                if (action == IZRED || action == DERED || clips[i].isRedStart() || clips[i].isRedWidth()){
                    //Main.logger.info(SizeString("Dur. actual: ",w,g,0));
                    g.setColor(verde_corporativo);
                    g.drawString(SizeString("Dur. actual: ",w,g,0),start+6,0+65);
                    g.setColor(Color.BLACK);
                    Multitime timeSelected = new Multitime();
                    timeSelected.setPixels(clips[i].getWidth(),DAY_MAX_TIME);
                    g.drawString(SizeString(getStringFromDuration(timeSelected.getSeconds()),w,g,70),start+6+70,0+65);
                }   
//                }
//                g.drawString(String.valueOf(clips[i].getID()),start+6,0+60);
//                g.drawString(String.valueOf(i),start+6,0+70);
                Multitime tstart = new Multitime();
                tstart.setPixels(clips[i].getStart(),DAY_MAX_TIME);
//                //Main.logger.info("tstart.getSeconds()");
//                //Main.logger.info(tstart.getSeconds());
                g.drawString(beginTime(tstart.getSeconds()),start,altura+10);   
            }
        }
        //selected Clip
        if (isClipSelected){
            w = getVirtualWidth(clipSelected.getWidth());
//            if (clipSelected.isSplited()){
//                w = clipSelected.getSplitWidth();
//            }
            
            name = clipSelected.getName();
            if (clipSelected.isRedStart() && action==IZRED)
                start = getVirtualStart(clipSelected.getStart() -mark);
            else{
                start = getVirtualStart(clipSelected.getMoveStart())-mark;
            }
            g.setColor(Color.WHITE);
            g.fillRect(start, 0, w, altura);
            g.setColor(verde_corporativo);
            g.fillRect(start+5, 0+5, w-10, altura-10);
            g.setColor(black_green);
            //____________________________________
            //Multitime steptime = new Multitime((int)clipSelected.getDuration());
            steptime.setTime((int)clipSelected.getDuration());
            int steps;
                    try{
                        steps = w / steptime.getPixels(max_time);
                    }catch(java.lang.ArithmeticException e){
                        steps = 0;
                   }
            for (i=1;i <= steps;i++){
                    begin = ((start + i*steptime.getPixels(max_time)));
                    //g.setColor(new Color(171,178,111));
                    //g.drawLine(begin,height-10,begin,0);
                    int k=0;
                    for (k=1;k < points*2; k++)
                        if (k%2 ==0)
                            if (steps > 1)
                                g.drawLine(begin,k*(altura)/(points*2),begin,k*(altura)/(points*2)+1);
                    //g.setColor(Color.WHITE);
                    //g.drawString(Integer.toString(i)+"h",begin,height-10);
                }
            //____________________________________
            String[] ruta = clipSelected.getName().split("/");
            String sname = ruta[ruta.length-1];
            //int width_string = clipSelected.getName().length()*5;

            g.setColor(yellow_negative);
            g.drawString(SizeString("Medio: ",w,g,0),start+6,0+22);
            g.setColor(Color.WHITE);
            g.drawString(SizeString(sname,w,g,6),start+6+6,0+35);
            g.setColor(yellow_negative);
            g.drawString(SizeString("Duracion: ",w,g,0),start+6,0+50);
            g.setColor(Color.WHITE);
            g.drawString(SizeString(getStringFromDuration(clipSelected.getDuration()),w,g,60),start+6+60,0+50);
            if (action == IZRED || action == DERED || clipSelected.isRedStart() || clipSelected.isRedWidth()){
                g.setColor(yellow_negative);
                g.drawString(SizeString("Dur. actual: ",w,g,0),start+6,0+65);
                g.setColor(Color.WHITE);
                Multitime timeSelected = new Multitime();
                timeSelected.setPixels(clipSelected.getWidth(),DAY_MAX_TIME);
                g.drawString(SizeString(getStringFromDuration(timeSelected.getSeconds()),w,g,70),start+6+70,0+65);
            }
            
//            g.drawString(String.valueOf(clipSelected.getID()),start+6,0+60);
//            g.drawString(String.valueOf(i),start+6,0+70);
            g.setColor(Color.BLACK);
    //        g.drawString(beginTime(pixelsToTime(clipSelected.getStart())),start,altura+10);   
            Multitime tstart = new Multitime();
            //Main.logger.info("clipSelected.getStart()");
            //Main.logger.info(clipSelected.getStart());
            //Main.logger.info((long)clipSelected.getStart()*(long)DAY_MAX_TIME/(long)max_time);
            tstart.setPixels(clipSelected.getStart(),DAY_MAX_TIME);
            g.drawString(beginTime(tstart.getSeconds()),start,altura+10);   
        }
    }
    
    /**
     * get the factor throught we know the exact coordinates where we are working
     * respect to width and the horizontal scroll of widget parent.
     *
     * @param width     The width of parent widget
     * @param loc       The X coordinate where the file is added in the widget
     * @param marca     The value of the offset of time line across the parent widget
     */
    public int factorSlide(){
        //int ratio = (width)/max_time;
//        if ((mark*(max_time-width))/100 >= max_time)
//            return max_time;
        if (zoom != WEEK)
            return (mark * (max_time-width))/MAX_VALUE_SLIDER1;
            //return (((mark*max_time)/100)*DAY_MAX_TIME)/max_time;
        else{
            
            //return ((mark*(max_time-width))/100)/14;
            return 0;
        }
//          if (zoom == WEEK)
//            return 0;
//        else
//            //return (mark*(max_time-width)) +loc;
//            return ((mark*max_time)/100)*DAY_MAX_TIME/max_time;
        
    }
    
    /**
     * for backward compatibility
     *
     *
     */
    
    public int factorSlide(int loc){
        //int ratio = (width)/max_time;
//        if ((mark*(max_time-width))/100 >= max_time)
//            return max_time;

//        if (zoom==DAY)
//            return (mark*(DAY_MAX_TIME-width))/100 +loc;
//        else{
//
//            //return ((mark*(max_time-width))/100)/14;
//            return (int)(((long)loc*(long)DAY_MAX_TIME)/(long)WEEK_MAX_TIME);
//        }
        if (zoom == WEEK)
            return (int)(((long)loc*(long)DAY_MAX_TIME)/(long)WEEK_MAX_TIME);
        else
            //return (mark*(max_time-width)) +loc;
            return (int)((((long)mark*((long)max_time-width))/MAX_VALUE_SLIDER1*1.0+(long)loc)*(long)DAY_MAX_TIME/(long)max_time);
    }
    
    /**
     * change start in seconds to a String with format: hh:mm
     *
     * @param seconds seconds to format to hh:mm
     */
    private String beginTime(long seconds){
        int minutes_t = 24*60;
        seconds = seconds % (24*60*60);
        long minutes = seconds / 60;
        String fill;
        if (minutes%60 < 10)
            fill = "0";
        else
            fill = "";
        //String cadena = Long.toString(minutes/60)+":"+fill+Long.toString(minutes%60);
        return Long.toString(minutes/60)+":"+fill+Long.toString(minutes%60);
    }
    
    /**
     * mark a Clip as Selected
     * 
     * 
     * @param seconds seconds to format to hh:mm
     */
    public void clipSelect(Clip selected){
        clipDeselect();
//        if ((selected.isSplited() && (selected.getSplitedFrom() == -1)) || selected.isSplited()==false){
            clipSelected = selected;
            isClipSelected = true;
            clipSelected.setVisible(false);
//        }
    }
    
    /**
     * undo the seleccion of a Clip
     */
    public void clipDeselect(){
        try{
    //probando        clipSelected.setMoveStart(clipSelected.getStart());
            clipSelected.setVisible(true);
            clipSelected = null;
            isClipSelected = false;        
        }
        catch(java.lang.NullPointerException e){
            
        }
    }
    
    /**
     * mark a Clip as Selected
     * 
     * 
     * @param seconds seconds to format to hh:mm
     */
    public void updateContext(int mark,int width){
        this.width = width;
        this.mark = mark;
        if(zoom == WEEK){
            WEEK_MAX_TIME = width;
            max_time = WEEK_MAX_TIME;
            REDIMENSION = REDI_WEEK;
        }
        else{
            REDIMENSION = REDI_DAY*zoom;
        }
    }
    
    /**
     * set the pointer to the next day
     *
     * @param collection the day after
     */
    public void setTomorrow(ClipCollection collection){
        tomorrow = collection;
    }
    
    public int getNumClips(){
        return ocupados;
    }
    
    public Clip first_iter(){
        for(int i=0; i < DAY_MAX_TIME; i++)
            if(occuped[i]!= -1){
                iter = i;
                return clips[occuped[i]];
            }
        return clips[0] ;
    }
    
    public Clip next_iter(){
        for(int i=iter; i < DAY_MAX_TIME; i++)
            if(occuped[i]!= occuped[iter] && occuped[i]!= -1){
                iter = i;
                return clips[occuped[i]];
            }
        return clips[0] ;
    }
    
    public void redClip(Clip clipy,int posicion, int action){
        if (action == IZRED){
            clipy.setRedStart(posicion);
        }
        else if (action==DERED)
            clipy.setRedWidth(posicion);   
    }
    
    public void OriginalDur(int loc){
        action= DELRED;
        Clip clipy = getClipByLocation(loc);
        desoccupeRed(clipy);
        clipy.setOriginalSize();
        
        occupe(clipy);
        //CONTEMPLAR EL SPLITAGE
    }
    
    public void setZoom(int z){
        switch (z){
            case 1:
                zoom = DAY;
                max_time = DAY_MAX_TIME;
                break;
            case 10:
                zoom = WEEK;
                max_time = WEEK_MAX_TIME;
                break;
            default: 
                zoom = z;
                max_time = (DAY_MAX_TIME*(10-z))/20;
                break;
        }
    }
    
    public int getZoom(){
        return zoom;
    }
    
    private String getStringFromDuration(double dur){
        //double dur = clipy.getDuration();
        String sdur = "";
        if(dur>3600){
            sdur += Long.toString((long)dur/3600)+" h ";
            dur = dur - ((long)dur/3600)*3600;
        }
        if(dur>60){
            sdur += Long.toString((long)dur/60)+" m ";
            dur = dur - ((long)dur/60)*60;
        }
        sdur += Long.toString((long)dur)+" s";
        return sdur;
    }
    
    private int getVirtualWidth(int w){
//        if(zoom == DAY)
//            return w;
//        else{
//            //return (w/14);
//            return (w*WEEK_MAX_TIME)/DAY_MAX_TIME;
//        }
        return getVirtualStart(w);
    }
    
    private int getVirtualStart(int w){
//        if(zoom == DAY)
//            return w;
//        else
//            return (w*WEEK_MAX_TIME)/DAY_MAX_TIME;
//        //Main.logger.debug("(w*max_time)/DAY_MAX_TIME");
//        //Main.logger.debug((long)((long)w*(long)max_time)/(long)DAY_MAX_TIME);
//        //Main.logger.debug((int)(((long)w*(long)max_time)/(long)DAY_MAX_TIME));
        //return (w*max_time)/DAY_MAX_TIME;
        return (int)(((long)w*(long)max_time)/(long)DAY_MAX_TIME);
    }
    
    private String SizeString(String orig, int w, Graphics g, int offset){
            java.awt.geom.Rectangle2D  rectangulo =  g.getFontMetrics().getStringBounds(orig,0,orig.length()-1,g);
            int index=0;
            while (rectangulo.getWidth() > w-18-offset){
                index++;
                if(orig.length()-1 < index )
                    return "";
                rectangulo =  g.getFontMetrics().getStringBounds(orig,0,orig.length()-1-index,g);
             }
            return orig.substring(0,orig.length()-index);
    }
    

    public Clip[] getClips() {
        return clips;
    }
    
    public String [] getClipNames(){
        String [] res = null;
        if(this.getNumClips() != 0){
            res = new String [this.getNumClips()];
            for(int i=0; i<this.getNumClips(); i++){
                res[i] = this.clips[i].getName();
                if(System.getProperty("os.name").toUpperCase(Locale.ENGLISH).indexOf("WINDOWS") != -1)
                    res[i] = res[i].replaceAll("/","\\\\");
            }
        }
        return res;
    }
    
    public boolean isClipConsecutive(Clip clipy){
        
        for(int i =clipy.getStart()-1; i > clipy.getStart()-6 && i>= 5; i--){
            if(occuped[i]!= -1)
                return true;
        }
        return false;
    }
    
    public int[] getOccupes(){
        return occuped;
    }
//    
//    private int timeToPixels(int s){
//        return s*max_time/604800;
//    }
//    
//    private int pixelsToTime(int p){
//        return p*604800/max_time;
//    }
    
    /*clips: is the list of clips, not ordered, the ID is the same that the
     *   posicion in array*/
    private  Clip[] clips;
    //t_clips: list of clips for template
    public SimpleClip[] t_clips;
    //c_clips: list of clips for template content
    public SimpleClip[] c_clips;
    //free: represent the free IDs in clips array
    private  boolean [] free;
    /*occuped: represents the points ocuped in the draw area. Each point reference
        the ID of the Clip that occupes it*/
    private int [] occuped;
    /*clipSelected: a reference to the Clip selected, if there isnt a Clip selected, keep the
     * last selected*/
    private  Clip  clipSelected;
    //isClipSelected: if there is a Clip selected
    private static boolean isClipSelected;
    //ocupados: keep the number of clips
    private int ocupados;
    //verde_corporativo: define the main color, aside the White.
    private Color verde_corporativo;
    //yellow_negative: degine the color of titles un selected clips
    private Color yellow_negative;
    //black_green: degine the color of redimension of clips.
    private Color black_green;
    private Color reserved;
    //max_time: the number of pixels in the draw area
    private int max_time=1992*7;
    
    //private static final int DAY_MAX_TIME = 1992*7;
    //  1992*7*20 = 278880
    public static final int DAY_MAX_TIME = 1992*7*20;
    //private int WEEK_MAX_TIME = 1992;
    public static int WEEK_MAX_TIME = 985;
    //zoom: the zoom level in the timeline,min 1 and max 7
    public  int zoom = 7;
    //ratio_horas: number of pixels/hour
    private int ratio_horas=max_time/(24);
    //rayas: number of lines in half-hours sepparations
    private final int rayas = 10;
    //points: number of points in half-hours sepparations
    private final int points = 20;
    //MAX_NUM_CLIPS: max number of clips, it needed for static array
    public static final int MAX_NUM_CLIPS = 2000;
    //sticky_umbral: number of pixels to make sticky movement between clips
    private final int sticky_umbral = 10;
    //dPlayer: is the JDesktopPane where is contained the player
    private static JDesktopPane dPlayer;
    /*clipjmf: is the instance of playerJFM, the JInternalFrame that contain
     *the player*/
    private static PlayerJMF clipjmf;
    /*clipjmf_aux: is an aux var used in add a Clip*/
    private static PlayerJMF clipjmf_aux;
    //playing: if there is a video playing or not
    private static boolean playing;
    //splited_clip: 
    private Clip splited_clip;
    //tomorrow: a pointer to next day collection
    private ClipCollection tomorrow;
    //supportedFormats: list of supported format by extensions
    private final String [] supportedFormats = {"avi","ogg","mov","flv","mpg","mp4"};
    private final int COPYID = 5000;
    private static int mark;
    private static int width=1047;
    private  int moving_old_start;
    //day: the day represent the object
    private int day;
    private int iter;
    public final int DELRED = 4;
    public final int MOVING = 1;
    public final int IZRED = 2;
    public final int DERED = 3;
    public int action = 0;
    public final int DAY = 1;
    public final int DAYMIDDLE = 2;
    public final int WEEK = 10;
    private final String[] semana;
    private final int REDI_DAY=15;
    private final int REDI_WEEK=200;
    public int REDIMENSION = REDI_WEEK;
    private Multitime steptime;
    private Skin skin;
    public static final int MAX_VALUE_SLIDER1 = 500;
    //private final int MIN_INF_WIDTH = 150;
    private TemplateManager template_manager;
    private Gui parent;
}


    