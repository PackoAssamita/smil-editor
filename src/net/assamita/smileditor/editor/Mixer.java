/*
 * Mixer.java
 *
 * Created on 26 de octubre de 2007, 9:58
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 

  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;

/**
 * Do the mixer between the template media list and the media inserted by the user, and
 * create a list of Clip that will be formated into SMIL.
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */
public class Mixer {
    
    /** Creates a new instance of Mixer 
     * 
     * @param localclips    list of clips from user
     * @param ocuppeds      number of clips
     * @param remoteclips   list of clips from template
     * @param mask          mask to modify permissions
     * @param occup         vector where each element
     *        value is the id of the clip that occupe the position or -1 if the position is free
     */
    public Mixer(Clip[] localclips,int ocuppeds,SimpleClip[] remoteclips, SimpleClip[] mask ,int[] occup) {
        list_clips_in = localclips;
        ocupados = ocuppeds;
        list_remote_in = remoteclips;
        list_clips_out = new Clip[ClipCollection.MAX_NUM_CLIPS];
        //local_occuped = occup.clone();
        local_occuped = occup;
        //ERROR = false;
        int i=0;
        remote_occuped = new int[ClipCollection.DAY_MAX_TIME];
        mask_occuped = new int[ClipCollection.DAY_MAX_TIME];
        for (i=0;i< ClipCollection.DAY_MAX_TIME; i++){
            remote_occuped[i]=-1;
            mask_occuped[i]=-1;
        }
        occupe(remoteclips,remote_occuped);
        occupe(mask,mask_occuped);
    }
    
    /**
     * do the main action of the class
     *
     * @return a Clip list with mixed clips
     */
    public Clip[] doMixer(){
        int index_begin=0;
        index_clips_out=0;
        old=0;
        id_local=-1; 
        id_remote=-1;
        
        //for first position:
        if(mask_occuped[0] == -1 && local_occuped[0] != -1){
            initiateToLocal();
        }
        else{
            initiateToRemote();
        }
        
        // rest positions
        for(int i=1; i < mask_occuped.length; i++){
            if(mask_occuped[i] != -1){
                //mascara ocupada, solo contenido remoto
                if(old==REMOTE){
                    //se sigue con contenido remoto
                    //index_end++;
                    if (mask_occuped[i] != id_remote){
                        //CODIFICAR
                            //index_clips_out += 1;
                        changeTo(REMOTE,i);
                        
                    }
                }
                else{            
                    //pasa de tener contenido local a contenido remoto
                    list_clips_out[index_clips_out] = new Clip(getClipById(local_occuped[i]));
                    list_clips_out[index_clips_out].setRedStart(index_begin);
                    list_clips_out[index_clips_out].setRedWidth(i);
                    index_begin = i;

                    //revisar puede faltar algo
                    
                    index_clips_out += 1;
                    old = REMOTE;
                }
            }
            else{
                if(local_occuped[i] == -1){
                    //no hay mascara pero no hay contenido local, se pone contenido remoto
                    if(old==REMOTE){
                        //se sigue poniendo el contenido remoto
                        //index_end++;
                        if (remote_occuped[i] != id_remote){
                            //CODIFICAR
                            //index_clips_out += 1;
                            id_remote = remote_occuped[i];
                            old= REMOTE;
                        }
                    }
                    else{
                        //se pasa de contenido local a contenido remoto
                        list_clips_out[index_clips_out] = new Clip(getClipById(local_occuped[i]));
                        list_clips_out[index_clips_out].setRedStart(index_begin);
                        list_clips_out[index_clips_out].setRedWidth(i);
                        index_begin = i;

                        //revisar puede faltar algo

                        index_clips_out += 1;
                        old = REMOTE;
                    }
                }
                else{
                    //no hay mascara y hay contenido local, se pasa a poner éste.
                    if(old==REMOTE){
                        //se passa de contenido remoto a poner contenido local
                        //cambiar
                        list_clips_out[index_clips_out] = new Clip(getSimpleClipById(local_occuped[i]));
                        list_clips_out[index_clips_out].setRedStart(index_begin);
                        list_clips_out[index_clips_out].setRedWidth(i);
                        index_begin = i;

                        //revisar puede faltar algo

                        index_clips_out += 1;
                        old = LOCAL;
                    }
                    else{
                        if (list_clips_in[i].getID() != id_remote){
                            System.out.println("dentro de ser contenido contenido local se cambia de medio");
                        }
                        //se continua poniendo contenido local
                        //index_end++;
                    }
                }
                
            }
        }
        
        return list_clips_out;
    }
    
    private void initiateToLocal(){
        old = LOCAL;
        id_local = local_occuped[0];
    }
    
    private void initiateToRemote(){
        old = REMOTE;
        id_remote = remote_occuped[0];
    }
    
    private void changeTo(int dest,int index){
        if (dest==REMOTE){
            id_remote = mask_occuped[index];
            old= REMOTE;
        }
        else{
            //id_local = 
            old = LOCAL;
        }
    }
    
    /**
     * fill the vector: vector with the media times in list.
     *
     * @param   vector vector size is ClipCollection.DAY_MAX_TIME and each element
     * value is the id of the clip that occupe the position or -1 if the position is free
     *
     * @param   list a list of SimpleClips
     *
     */
    private void occupe(SimpleClip [] list, int [] vector){
        Multitime time = new Multitime();
        int start=0,end=0,index=0;
        for (int i =0; i< list.length ; i++){
            start =0;
            end = 0;
            time.setTime(list[i].begin);
            if(list[i].begin != -1){
                if(i == 0){
                    start = 0;
                }
                else
                    start = index;
            }
            else{
                start = time.getPixels();
            }
            time.setTime(list[i].end - list[i].startClip);
            end = time.getPixels();
            if(end >= ClipCollection.DAY_MAX_TIME)
                end = ClipCollection.DAY_MAX_TIME-1;
            for(int j=start; j< end; j++)
                vector[j]= list[i].id;
            index = end;
        }
    }
    
    /**
     * get the Clip with the id: <b>id</b>, from the <i>list_clips_in</i>
     *
     * @param id the id to search in list
     * @return the Clip with id: <b>id</b>
     */
    private Clip getClipById(int id){
        for(int i=0; i< ocupados ; i++ ){
            if (list_clips_in[i].getID() == id)
                return list_clips_in[i];
        }
        return list_clips_in[0];
    }
    
    /**
     * get the SimpleClip with the id: <b>id</b>, from the <i>list_remote_in</i>
     *
     * @param id the id to search in list
     * @return the SimpleClip with id: <b>id</b>
     */
    
    private SimpleClip getSimpleClipById(int id){
        for(int i=0; i< list_remote_in.length ; i++ ){
            if (list_remote_in[i].id == id)
                return list_remote_in[i];
        }
        return list_remote_in[0];
    }
    
    
    
    private int [] local_occuped;
    private int [] remote_occuped;
    private int [] mask_occuped;
    private int LOCAL=1;
    private int REMOTE=2;
    private Clip [] list_clips_out;
    private Clip [] list_clips_in;
    private SimpleClip [] list_remote_in;
    private int old,id_local,id_remote;
    //public boolean ERROR;
    /**
     * number of elements mixed
     */
    public int index_clips_out;
    /**
     * vector where each element value is the id of the clip that occupe the position or -1 if the position is free
     */
    public int ocupados;
    
}
