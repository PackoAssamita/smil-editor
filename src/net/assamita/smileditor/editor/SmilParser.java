/*
 * SmilParser.java
 *
 * Created on 27 de julio de 2007, 14:29
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
(C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author packo
 */
public class SmilParser {
    
    /**
     * Creates a new instance of SmilParser
     */
    public SmilParser(String path, boolean reserve) {
        ERRORS = false;
        DAY_MAX_TIME = ClipCollection.DAY_MAX_TIME;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
                builder = factory.newDocumentBuilder();
                document = builder.parse(new File(path));
                videos = document.getElementsByTagName("video");
        }catch(ParserConfigurationException e){
                System.err.println("No se ha podido crear una instancia de DocumentBuilder");
                ERRORS = true;
        }catch(SAXException e){
                System.err.println("Error SAX al parsear el archivo");
                ERRORS = true;
        }catch(IOException e){
                System.err.println("Se ha producido un error de entrada salida");
                ERRORS = true;
        }

        if(ERRORS == false){
             if (videos.getLength() < 1)
                ERRORS = true;
            clips = new SimpleClip[numNotConsecutiveMedia()];
        }
        reserved = reserve;
    }
    
    //REVISAR Y QUITAR VARIABLES NO USADAS
    private int numNotConsecutiveMedia(){
        int tlength = 0;
        String begin;
        String end;
        String src;
        String startClip;
        String id;
        String repCount;
        int wid=0,istartClip,ibegin,iend,numConcat=0,t_numConcat=0;
        
        int ncol = 0;
        
        for(int i=0; i<videos.getLength(); i++){
            numConcat = 0;
            Element e = (Element)videos.item(i);
            begin = e.getAttribute("begin");
            end = e.getAttribute("end");
            src = e.getAttribute("src");
            startClip = e.getAttribute("startClip");
            id = e.getAttribute("id");
            repCount = e.getAttribute("repCount");
            int offset=-1;
            
            for (int j=i+1;j< videos.getLength(); j++){
                
                Element o = (Element)videos.item(j);

                if(o.getAttribute("id").compareTo(id)==0 && o.getAttribute("startClip").length()==0){
                    numConcat++;
                    if(o.getAttribute("end")!=""){
                        offset = Integer.valueOf(o.getAttribute("end")).intValue();
                    }
                    else
                        offset = -1;
                }
                else
                    break;
                
            }
            t_numConcat += numConcat;
            i += numConcat;
        }
        return videos.getLength() - t_numConcat;
    }
    
    
    public SimpleClip[] getClipsList(){
        
        int tlength = 0;
        String begin;
        String end;
        String src;
        String startClip;
        String id;
        String repCount;
        int wid=0,istartClip,ibegin,iend,numConcat=0,t_numConcat=0,index=0,irepCount;
        
        int ncol = 0;
        
        for(int i=0; i<videos.getLength(); i++){
            index = i - t_numConcat;
            numConcat = 0;
            Element e = (Element)videos.item(i);
            begin = e.getAttribute("begin");
            end = e.getAttribute("end");
            src = e.getAttribute("src");
            startClip = e.getAttribute("startClip");
            id = e.getAttribute("id");
            repCount = e.getAttribute("repCount");
            int offset=-1;
            
            for (int j=i+1;j< videos.getLength(); j++){
                Element o = (Element)videos.item(j);
                if(o.getAttribute("id").compareTo(id)==0 && o.getAttribute("startClip").length()==0){
                    numConcat++;
                    if(o.getAttribute("end").compareTo("") != 0){
                        offset = Integer.valueOf(o.getAttribute("end")).intValue();
                    }
                    else
                        offset = -1;
                    if (o.getAttribute("repCount").compareTo("")!=0)
                        repCount = o.getAttribute("repCount");
                }
                else
                    break;
            }
            t_numConcat+=numConcat;
            if (end.compareTo("") == 0)
                end = "-1";
            if (begin.compareTo("") == 0)
                begin = "-1";
            if (startClip.compareTo("") == 0)
                startClip = "-1";
            if (repCount.compareTo("") == 0)
                repCount = "0";
            
            istartClip = Integer.valueOf(startClip).intValue();
            ibegin     = Integer.valueOf(begin).intValue();
            iend       = Integer.valueOf(end).intValue();
            irepCount  = Integer.valueOf(repCount).intValue();
            
//            if (ibegin != -1){
//                Multitime btime = new Multitime(ibegin);
//                int pxstartClip = btime.getPixels(DAY_MAX_TIME);
             clips[index] = new SimpleClip(src,index,ibegin,istartClip,iend,numConcat+irepCount,offset);

            if (reserved == true)
                clips[index].recalculate_reserved();
            i = i+numConcat;    
        }
        
        return clips;
    }
    
    private NodeList videos;
    private DocumentBuilder builder;
    private Document document;
    private SimpleClip[] clips;
    private final int SECDAY = 86400;
    private int DAY_MAX_TIME ;
    private boolean reserved;
    public boolean ERRORS;
}
