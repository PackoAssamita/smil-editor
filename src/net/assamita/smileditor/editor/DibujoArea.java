/*
 * DibujoArea.java
 *
 * Created on 19 de febrero de 2007, 13:54
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
  
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */
package net.assamita.smileditor.editor;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.InvocationTargetException;
import javax.swing.*;
import java.lang.String;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import java.io.*;

/**
 *
 * @author packo
 */
public class DibujoArea extends JPanel implements DropTargetListener
{
    DropTarget dropTarget = null;
    
    /**
     * Creates a new instance of DibujoArea
     */
    public DibujoArea(JDesktopPane dPlayer, Gui padrec) {
        padre = padrec;
        init_componentes();
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                dibujoareaMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                dibujoareaMouseMoved(evt);
            }
        });
        super.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(MouseWheelEvent e) {
                int zoom = coleccion.getZoom();
                zoom -= e.getWheelRotation();
                if (zoom >10){
                    zoom = 10;
                }
                else {
                    if (zoom < 1)
                        zoom = 1;
                }    
                padre.zoomChanged(zoom);  
            }
        });
        super.addMouseListener(new java.awt.event.MouseListener() {
            public void mouseClicked(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
             
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mousePressed(MouseEvent e) {
                //handle the Button1 press (left button)
                
                
                if(e.getButton()==e.BUTTON1){
                    if (coleccion.isClipByLocation(e.getX()) == true){
                        oldXpos = coleccion.factorSlide(e.getX());
                        if (oldXpos >=coleccion.getMaxTime())
                            oldXpos = coleccion.getMaxTime();
                        pressed = true;
                        clipmv = coleccion.getClipByLocation(e.getX());
                           // mirar si se esta redimensionando
                           if (oldXpos < clipmv.getStart()+coleccion.REDIMENSION && oldXpos > clipmv.getStart()){
                            setAction(IZRED);
                            offset = oldXpos-clipmv.getStart();
                            coleccion.clipSelect(clipmv);
                           }
                           else if(oldXpos > clipmv.getStart()+clipmv.getWidth()-coleccion.REDIMENSION && oldXpos < clipmv.getStart()+clipmv.getWidth()){
                                setAction(DERED);
                                offset = clipmv.getStart()+clipmv.getWidth()-oldXpos;
                                coleccion.clipSelect(clipmv);
                           }
                           else{
                              offset = e.getX()- clipmv.getStart();
                              coleccion.clipSelect(clipmv);
                              setAction(MOVING);
                           }
                        repaint();
                    }
                    else{
                        coleccion.clipDeselect();
                        repaint();   
                    }
                }
                //handle the Button3 press (right button)
                if(e.getButton()==e.BUTTON3){
                    if (coleccion.isClipByLocation(e.getX()) == true){
                        jPopupMenu1.setLocation(e.getXOnScreen(),e.getYOnScreen());
                        jPopupMenu1.setVisible(true);
                        x_menu = e.getX();
                    }
                }
            }
            public void mouseReleased(MouseEvent e) {
                if (pressed == true){
                    //
                    coleccion.releaseMovement();
                    repaint();
                }
                released = true;
                pressed = false;
            }
        });
        dropTarget = new DropTarget( this, this );
        mensaje = "";
        try {
//        dias_coleccion = new ClipCollection[7];
//        for(int i=0; i < 7 ; i++)
//            dias_coleccion[i] = new ClipCollection(dPlayer,i);
//        for(int i=0; i < 6; i++)
//            dias_coleccion[i].setTomorrow(dias_coleccion[i+1]);
//            
//        coleccion = dias_coleccion[0];
            coleccion = new ClipCollection(dPlayer,padrec);
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        
        
        marca = 0;
        
        pressed =false;
        //newMark(0);
    }
    
    /**
     * define and initialize the components: the menu of right button
     *
     */
    private void init_componentes(){
        //menu clips
        jPopupMenu1 = new javax.swing.JPopupMenu("Opciones de clip");
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        mi_stop    = new javax.swing.JMenuItem();
        mi_sep     = new javax.swing.JSeparator();
        mi_sep2     = new javax.swing.JSeparator();
        
        jMenuItem1.setText("Reproducir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        
        mi_stop.setText("Parar");
        mi_stop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mi_stopActionPerformed(evt);
            }
        });
        
         
        jMenuItem2.setText("Eliminar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        
        jMenuItem3.setText("Duracion Original");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        
        jPopupMenu1.add(jMenuItem1);
        jPopupMenu1.add(mi_stop);
        jPopupMenu1.add(mi_sep);
        jPopupMenu1.add(jMenuItem2);
        jPopupMenu1.add(mi_sep2);
        jPopupMenu1.add(jMenuItem3);
        
//        //hora justa
//        horaJusta = new javax.swing.JPanel();
//        jLabel1 = new javax.swing.JLabel();
//        jSpinner1 = new javax.swing.JSpinner();
//        jLabel2 = new javax.swing.JLabel();
//        jSpinner2 = new javax.swing.JSpinner();
//        jButton1 = new javax.swing.JButton();
//        
//         jLabel1.setText("Hora: ");
//
//        jLabel2.setText(":");
//
//        jButton1.setText("Aceptar");
//
//        javax.swing.GroupLayout horaJustaLayout = new javax.swing.GroupLayout(horaJusta);
//        horaJusta.setLayout(horaJustaLayout);
//        horaJustaLayout.setHorizontalGroup(
//            horaJustaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGroup(horaJustaLayout.createSequentialGroup()
//                .addComponent(jLabel1)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//                .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
//                .addGap(2, 2, 2)
//                .addComponent(jLabel2)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//                .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//                .addComponent(jButton1)
//                .addContainerGap(24, Short.MAX_VALUE))
//        );
//        horaJustaLayout.setVerticalGroup(
//            horaJustaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGroup(horaJustaLayout.createSequentialGroup()
//                .addGap(21, 21, 21)
//                .addGroup(horaJustaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
//                    .addComponent(jLabel1)
//                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
//                    .addComponent(jLabel2)
//                    .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
//                    .addComponent(jButton1))
//                .addContainerGap(22, Short.MAX_VALUE))
//        );
//        horaJusta.setVisible(true);
    }
    
    //Callbacks
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
        
        coleccion.playClip(x_menu);
        jPopupMenu1.setVisible(false);
        repaint();
        //horaJusta.setVisible(true);
    }   
    
    private void mi_stopActionPerformed(java.awt.event.ActionEvent evt) {
        
        coleccion.stopClip();
        jPopupMenu1.setVisible(false);
        repaint();
        //horaJusta.setVisible(true);
    }   
    
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        coleccion.deleteClip(x_menu);
        repaint();
    }   
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        coleccion.OriginalDur(x_menu);
        jPopupMenu1.setVisible(false);
        repaint();
    }
    
    /**
     * overrides paintComponent method of jPanel
     *
     * @param g     graphic context
     */
    public void paintComponent(Graphics g) {
        gext = g;
        coleccion.draw(gext,super.getHeight());
    }
    
    /**
     * manage the dragged of file media, is a callback
     */
     private void dibujoareaMouseDragged(java.awt.event.MouseEvent e) {
         if (pressed==true){
                   //int posicion = coleccion.factorSlide()+e.getX();
                    int posicion;
                    
                        posicion = coleccion.factorSlide(e.getX());
                   
                   //int offsetWeek = posicion - clipmv.getMoveStart() ;
                   
                   if (action == MOVING)                   {
                       if(coleccion.zoom == coleccion.DAY)
                            coleccion.moveClip(clipmv ,clipmv.getMoveStart() + posicion - oldXpos );
                       else{
//                           if (posicion-oldXpos > clipmv.getWidth())
//                               oldXpos = 0;
                           
                           coleccion.moveClip(clipmv ,  posicion + clipmv.getMoveStart() -oldXpos   );
                       }
                       repaint();
                       oldXpos = posicion;
                   }
                   else if (action == IZRED){
                           if((clipmv.getStart()+clipmv.getWidth()) - (posicion - offset)  > coleccion.REDIMENSION){
                                coleccion.redClip(clipmv,posicion-offset,IZRED);
                           }
                           repaint();
                           //creo que va:
                           //oldXpos = posicion;
                   }
                   else if (action == DERED){
                       if( (posicion - offset) - (clipmv.getStart())   > coleccion.REDIMENSION)
                        coleccion.redClip(clipmv,posicion+offset,DERED);
                       repaint();
                   }
              }
    }
    private void dibujoareaMouseMoved(java.awt.event.MouseEvent e) {
    }
    
     // Step 4: Handle dragged object notifications
  public void dragExit(DropTargetEvent dte)
  {
    //System.out.println( "DT: dragExit" );
  }
  public void dragEnter(DropTargetDragEvent dtde)
  {
    //System.out.println( "DT: dragEnter" );
  }
  public void dragOver(DropTargetDragEvent dtde)
  {
    //System.out.println( "DT: dragOver" );
  }
  public void dropActionChanged(DropTargetDragEvent dtde)
  {
    //System.out.println( "DT: dropActionChanged" );
  }
// Step 5: Handle the drop
  public void drop(DropTargetDropEvent dtde)
  {
    //System.out.println( "DT: drop" );
    try
    {
      Transferable transferable = dtde.getTransferable();

      if( transferable.isDataFlavorSupported(
DataFlavor.stringFlavor ) )
      {
        // Step 5: Accept Strings
        dtde.acceptDrop( DnDConstants.ACTION_MOVE );

        // Step 6: Extract the Transferable String
        String s = ( String )transferable.getTransferData( 
DataFlavor.stringFlavor );
        addElement( s ,dtde.getLocation());
        // Step 7: Complete the drop and notify the DragSource 
//(will receive a 
        //     dragDropEnd() notification
        dtde.getDropTargetContext().dropComplete( true );
      }
      else
      {
        // Step 5: Reject dropped objects that are not Strings
        dtde.rejectDrop();
      }
    }
    catch( IOException exception )
    {
      exception.printStackTrace();
      System.err.println( "Exception" + exception.getMessage());
      dtde.rejectDrop();
    }
    catch( UnsupportedFlavorException ufException )
    {
      ufException.printStackTrace();
      System.err.println( "Exception" +
      ufException.getMessage());
      dtde.rejectDrop();
    }
  }

  public void addElement( Object s,Point loc ){
    mensaje = s.toString();
    coleccion.add(mensaje,super.getWidth(),loc.x , marca);
    repaint();
  }
  
  public void newMark(int mark){
      marca = mark;
      coleccion.updateContext(marca,super.getWidth());
      repaint();
  }
  public void changeDay(int day){
      coleccion.clipDeselect();
      //coleccion = dias_coleccion[day];
      //System.out.println("Dia cambiado");
      repaint();
  }
  
  public boolean saveSMIL(String filename){
        if(filename=="")
            smil_writer = new SmilWriter(SmilWriter.SAVE);
        else
            smil_writer = new SmilWriter(filename,SmilWriter.SAVE);
        smil_writer.newDocument("Media", "Creator","Autor");
        boolean clips = false;
        boolean consecutive= false;
        
        if(coleccion.getNumClips()>0){
            Clip iterclip = coleccion.first_iter();
            clips=true;
            if (coleccion.isClipConsecutive(iterclip))
                consecutive = true;
            else
                consecutive = false;
            smil_writer.addElement(iterclip,consecutive);
            for(int i=1; i < coleccion.getNumClips();i++){
                iterclip = coleccion.next_iter();
                if (coleccion.isClipConsecutive(iterclip))
                    consecutive = true;
                else
                    consecutive = false;
                smil_writer.addElement(iterclip,consecutive);
            }
        }    
        
        if(!clips){
            Info("No hay clips para guardar");
            return false;
        }
        smil_writer.closeDocument();
        return true;
    }
  
  /**
     * Show a info message. Need com.sun.media.ui.*
     *
     * @param s         The message to show.
     */
    static void Info(String s) {
        InfoBox mb = new InfoBox(new Frame(),s,false);
     
    }
    
    public void restart(JDesktopPane dPlayer){
        coleccion.stopClip();
        mensaje = "";
        try {
//        dias_coleccion = new ClipCollection[7];
//        for(int i=0; i < 7 ; i++)
//            dias_coleccion[i] = new ClipCollection(dPlayer,i);
//        for(int i=0; i < 6; i++)
//            dias_coleccion[i].setTomorrow(dias_coleccion[i+1]);
//            
//        coleccion = dias_coleccion[0];
            coleccion = new ClipCollection(dPlayer,padre);
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
//        fondo = new Color(214,223,136);
        
        marca = 0;
        
        pressed =false;
    }
    
    private void setAction(int act){
        action = act;
        coleccion.action = action;
    }
    
    public void makeZoom(int z){
        newMark(marca);
        coleccion.setZoom(z);
        repaint();
    }
    
    public ClipCollection getColeccion(){
        return coleccion;    
    }
    
//    public void updateTemplate(boolean err){
//        //repaint();
//        //padre.updateTemplate();
//        //padre.repaint();
//       // newMark(marca);
//        //repaint();
//    }
    
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem mi_stop;
    private javax.swing.JSeparator mi_sep;
    private javax.swing.JSeparator mi_sep2;
    private javax.swing.JPanel horaJusta;
    private javax.swing.JButton jButton1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private static java.lang.String mensaje;
    private static Graphics gext;
    private ClipCollection coleccion;
    //private ClipCollection[] dias_coleccion;
    //private Color fondo;
    private int marca;
    private int oldXpos;
    private boolean pressed;
    private boolean released;
    private Clip clipmv;
    private int x_menu;
    private  int offset;
    private SmilWriter smil_writer;
    //private int REDIMENSION = 15;
    public int NOACTION = 0;
    public int MOVING = 1;
    public int IZRED = 2;
    public int DERED = 3;
    public int action = NOACTION;
    private Gui padre;
    //private int offset=0;
}
