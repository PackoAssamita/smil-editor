/*
 * SimpleClip.java
 *
 * Created on 18 de octubre de 2007, 17:17
 * 
(C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.assamita.smileditor.editor;

import java.net.URL;

/**
 *
 * @author Francisco Jose Moreno Llorca <packo@assamita.net>
 */
public class SimpleClip {
    
    /** Creates a new instance of SimpleClip 
     * 
     * @param fichero   absolute file path
     * @param ident     id of clip
     * @param beg       begin of clip in SECONDS
     * @param istart    offset in beggining of clip in SECONDS
     * @param dur       duration of clip in SECONDS
     * @param rumrep    number of repetitions of clip
     * @param off       the duration of last repetition
     *
     */
    public SimpleClip(String fichero,int ident,int beg,int istart,int dur,int numrep,int off) {
        src = fichero;
        id = ident;
        if (beg > 0)
            begin = beg;
        if (istart > 0)
            startClip = istart;
        duration = -1;
        numRep = numrep;
        offset = off;
        end = dur;
        try{
            if ( fichero.indexOf("file:") != 0)
                url = new URL("file:"+fichero);
            else
                url = new URL(fichero);
        }
        catch ( java.net.MalformedURLException e ){
            
        }
        //isredstart
        //duration
    }
    
    public int getStart(){
        return start;
    }
    
    public void setDuration(int length){
        if (numRep > 0){
            if (offset > 0){
                duration = length - startClip + length*(numRep -1 ) + offset;
            }
            else{
                duration = length - startClip + length*(numRep);
            }
        }
        else{
            duration = length - startClip;
        }
    }
    
    public void recalculate(){
        assert duration != -1;
        Multitime time = new Multitime(duration);
        width = time.getPixels(ClipCollection.DAY_MAX_TIME);
        if (begin > 0 ){
            time.setTime(begin);
            
            start = time.getPixels(ClipCollection.DAY_MAX_TIME);
        }
        else
            begin = -1;        
        time.setTime(startClip);
        startClip = time.getPixels(ClipCollection.DAY_MAX_TIME);
    }
    
    public void recalculate_reserved(){
        assert duration != -1;
        Multitime time = new Multitime(end);
        width = time.getPixels(ClipCollection.DAY_MAX_TIME);
        if (begin > 0 ){
            time.setTime(begin);
            
            start = time.getPixels(ClipCollection.DAY_MAX_TIME);
        }
        else
            begin = -1;        
        time.setTime(startClip);
        startClip = time.getPixels(ClipCollection.DAY_MAX_TIME);
    }
    
    public void recalculate(int length){
        setDuration(length);
        recalculate();
    }
    
    public String src;
    public int id;
    public int begin=0;
    public int startClip=0;
    public int duration;
    public int numRep;
    public int offset;
    public int end;
    public URL url;
    
    public int start;
    public int width;
    
}
