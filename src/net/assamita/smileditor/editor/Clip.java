/*
 * Clip.java
 *
 * Created on 21 de febrero de 2007, 11:47
 *
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;

import com.omnividea.media.renderer.video.Java2DRenderer;
import java.net.URL;
import java.io.File.*;
import java.awt.*;

/**
 *
 * @author packo
 */
public class Clip {
    
    /**
     * Creates a new instance of Clip
     */
    public Clip(String fichero,int ident,int beg,int dur){
        try{
            if ( fichero.indexOf("file:") != 0)
                url = new URL("file:"+fichero);
            else
                url = new URL(fichero);
        }
        catch ( java.net.MalformedURLException e ){
            Fatal("Desde class:clip :"+e.toString());
        }

        DAY_MAX_PIXELS = ClipCollection.DAY_MAX_TIME;
        duration= dur;
        begin=beg;
        startClip=-1;
        name = url.getPath();
        width = (duration* DAY_MAX_PIXELS )/SECWEEK;
        start = beg;
        mvstart=start;
        id = ident;
        visible = true;
//        splited = false;
//        split_width = 0;
//        splited_from = -1;
        redi_start = -1;
        redi_width = -1;
        
//        redi = false;
    }
    
    /**
     * Creates a copy of Clip
     */
    public Clip(Clip copia){
        begin = copia.begin;
        duration = copia.duration;
        name = copia.name;
        width = copia.width;
        start = copia.start;
        startClip = copia.startClip;
        mvstart = start;
        visible=true;
        redi_start = copia.redi_start;
        redi_width = copia.redi_width;
        //id = copia.id + COPYID;
        id = copia.id;
        url = copia.url;
        //visible = copia.visible;
//        splited = copia.getSplited();
//        split_width = copia.getSplitWidth();
    }
    
    /**
     *Creates a copy of Clip based on a SimpleClip
     */
    public Clip(SimpleClip copia){
        Multitime time = new Multitime(copia.begin);
        begin = start = time.getPixels();
        try{
            if ( copia.src.indexOf("file:") != 0)
                url = new URL("file:"+copia.src);
            else
                url = new URL(copia.src);
        }
        catch ( java.net.MalformedURLException e ){
            Fatal("Desde class:clip :"+e.toString());
        }
        duration = copia.end;
        name = copia.src;
        time.setTime(duration);
        width = time.getPixels();
        
    }
    
     /**
     * Show a message, generally an error. Need com.sun.media.ui.*
     *
     * @param s         The message to show.
     */
     static void Fatal(String s) {
        MsgBox mb = new MsgBox(new Frame(),s,false);
    }
     
     /**
     * return if the file is splited in two days
     *
     * @param s         The message to show.
     */
//     public boolean getSplited() {
//        return splited;
//    } 
    
    /**
     * get the name of Clip
     * 
     * 
     * @return the name of Clip
     */
    public String getName(){
        return name;
    }
    
    /**
     * get the url in String
     *
     * @return      the url in String
     */
    public String getUrl(){
        return url.toString();
    }
    
    /**
     * get the width of Clip in the draw area, not the duration.
     * 
     * 
     * @return the url in String
     */
    public int getWidth(){
        return width;
    }
    
    /**
     * set the width of Clip in the draw area, not the duration.
     */
    public void setWidth(int w){
        width = w;
    }
    
    /**
     * get the end of the Clip in the draw area
     * 
     * 
     * @return the start point in the draw area
     */
    public int getEnd(){
        return start+width;
    }
    
    /**
     * get the start of the Clip in the draw area
     * 
     * 
     * @return the start point in the draw area
     */
    public int getStart(){
        return start;
    }
    
    /**
     * get the start of the Clip from playlist in week seconds
     * 
     * 
     * @return the start of the Clip from playlist in week seconds
     */
    public int getStartClip(){
        return startClip;
    }
    
    /**
     * set the start of the Clip from playlist in week seconds
     */
    public void setStartClip(int start){
        startClip = start;
    }
    
    /**
     * get the start of the Clip in the draw area when moving
     * 
     * 
     * @return the start point in the draw area when moving
     */
    public int getMoveStart(){
        return mvstart;
    }
    
    /**
     * set the start of Clip in the draw area.
     * 
     * 
     * @param begin     the url in String
     */
    public void setStart(int begin){
        mvstart = begin;
        start = begin;
    }
    
    /**
     * set the start of Clip in the draw area when moving.
     * 
     * 
     * @param begin     the url in String
     */
    public void setMoveStart(int begin){
        
        mvstart = begin;
    }
    
    
    /**
     * get the duration of the Clip in seconds
     * 
     * 
     * @return the url in String
     */
    public int getDuration(){
        return duration;
    }
    
    /**
     * get the ID of Clip
     * 
     * 
     * @return the url in String
     */
    public int getID(){
        return id;
    }
    
    /**
     * return if the Clip is visible
     * 
     * 
     * @return true if is visible, false in other case
     */
    public boolean isVisible(){
        return visible;
    }
    
//    public boolean isSplited(){
//        return splited;
//    }
    /**
     * set the visibiliti of the Clip
     * 
     * 
     * @param visibility   the visibility of the Clip
     */
    public void setVisible(boolean visibility){
        visible = visibility;
    }
    
    public String toString(){
        return "Comienzo: "+ String.valueOf(start) + "\nID: "+String.valueOf(id)+"\nVisible: "+String.valueOf(visible);
    }
    
//    public void setSplit(int width){
//        split_width = this.width;
//        splited = true;
//        this.width = width;
//    }
//    
//    public void setSplit(boolean activate){
//        if (!activate){
//            this.splited = false;
//            this.width = this.split_width;
//            this.split_width = 0;
//            this.splited_from = -1;
//        }            
//    }
//    
//    public int getSplitWidth(){
//        return split_width;
//    }
//    
//    public void setSplitedFrom(int id){
//        splited_from = id;
//    }
////    
//    public void setSplitWidth(int width){
//        this.width = width;
//    }
//    
//    public int getSplitedFrom(){
//        return splited_from;
//    }
//    
//    public void setSplitStart(int start){
//        this.start = start;
//    }
    
    public int getBegin(){
        return begin;
    }
    
    public void setRedStart(int redstart){
        //save the original size in redi_start
        if (redi_start != -1){
            width = width + start -redstart;
            start = redstart;
        }
        else{
            redi_start = (duration*DAY_MAX_PIXELS)/SECWEEK - java.lang.Math.abs(start-redstart);
            redi_start_width = width;
            start = redstart;
            width = width + start -redstart;
        }
    }
    
    public void setRedStartOpening(){
        //save the original size in redi_start
//        if (redi_start != -1){
//            width = width + start -redstart;
//            start = redstart;
//        }
//        else{
            redi_start = start;
            redi_start_width = width;
            //start = redstart;
            //width = width + start -redstart;
//        }
    }
    
    public void delRedStartMoving(){
        if (redi_start != -1){
            start = redi_start;
            width = redi_start_width;
            redi_start = -1;            
            redi_start_width = -1;
        }
    }
    
    //REVISAR
    public void delRedStart(){
        if (redi_start != -1){
            width = (duration*DAY_MAX_PIXELS)/SECWEEK;
            redi_start = -1;            
            redi_start_width = -1;
        }
    }
    
    public boolean isRedStart(){
        if (redi_start == -1)
            return false;
        else
            return true;
    }
    
    public void setRedWidth(int redwidth){
        if (redi_width != -1)
            width = redwidth-start;
        else{
            //redi_width = width;
            redi_width = redwidth;
            width = redwidth-start;
        }
    }
    
    public void setRedWidthOpening(int dur){
//        if (redi_width != -1)
//            width = redwidth-start;
//        else{
            //redi_width = width;
            Multitime time = new Multitime(dur);
            redi_width = width;
            width = time.getPixels(ClipCollection.DAY_MAX_TIME);
//        }
    }
    
    //REVGISAR
    public void delRedWidthMoving(){
        if (redi_width != -1){
            width = redi_width;
            redi_width = -1;            
        }
    }
    
    //REVGISAR
    public void delRedWidth(){
        if (redi_width != -1){
            width = (duration*DAY_MAX_PIXELS)/SECWEEK;
            redi_width = -1;            
        }
    }
    
    public boolean isRedWidth(){
        if (redi_width == -1)
            return false;
        else
            return true;
    }
    
    public int getRedStart(){
        return redi_start;
    }
    
    public int getRedStartWidth(){
        return redi_start_width;
    }
    
    public int getRedWidth(){
        return redi_width;
    }
    
    public void setOriginalSize(){
        delRedStart();
        delRedWidth();
    }
    
    public void setDuration(int sec){
        duration = sec;
    }
    
    // name: string with the url
    private String name;
    // name: URL of media
    private URL url;
    // width: the width of the Clip in the timeline
    private int width;
    // start: the position in the timeline of the Clip
    private int start;
    // startClip: the position in the timeline of the Clip from playlist
    private int startClip;
    // mvstart: represent the position of the Clip in the timeline while moving
    private int mvstart;
    // duration: the duration of the Clip in seconds
    private int duration;
    // id: identificator number
    private int id;
    // visible: if the Clip is visible or not
    private boolean visible;
    // splited: if the Clip is splited between day or not
    private boolean splited;
    // split_width: the width before split
    private int split_width;
    // splited_from: indicate the ID from the Clip is splited from
    private int splited_from;
    // COPYID: is added to a id for make a new id from a copy
    private final int COPYID = 5000;
    
    private int begin;
    
    private int redi_start_width = -1;
    
    private int redi_start = -1;
    
    private int redi_width = -1;
    
    //max_time: the number of pixels in the draw area
    private int DAY_MAX_PIXELS;
    //daytime: number of secons in a day;
    private final int DAYTIME=86400;
    //SECWEEK: seconds in a week
    public static final int SECWEEK = 604800;
}
