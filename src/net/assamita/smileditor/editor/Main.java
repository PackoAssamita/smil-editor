/*
 * Main.java
 *
 * Created on 19 de febrero de 2007, 12:38
 * @author      Francisco Jose Moreno Llorca <packo@assamita.net>
 * 
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.editor;


//import org.apache.log4j.Logger;
//import org.apache.log4j.net.*;
//import org.apache.log4j.BasicConfigurator;
/**
 *
 * @author packo
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //logger = Logger.getLogger(Main.class);
        //logger.addAppender(new SocketAppender("localhost",4560));
        Gui guiapp;
        // TODO code application logic here
//        try{
        guiapp = new Gui();
        guiapp.setVisible(true);
//        }catch(java.lang.Exception e){
//            System.out.println("EXCEPTION: "+e.toString());
//        }
//        
    }
    //public static Logger logger;
    
}
