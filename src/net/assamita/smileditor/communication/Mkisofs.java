/*
 * Mkisofs.java
 *
 * Created on 7 de septiembre de 2007, 13:30
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Vector;

/**
 * Creación de imagenes iso
 * @author Fco. Jesus Gonzalez Mata
 */
public class Mkisofs{
  
    public static String SO_WIN = "WINDOWS";
    public static String SO_LINUX = "LINUX";
    public static String SO_MAC = "MAC";
    private String cmd_borrar = null;
    
    /** Nombre del archivo playlist */
    private String playlist;
    /** Nombres de los archivos multimedia */
    private String [] medios;
    /** Nombre de la imagen que se va a crear */
    private String imagen = "prueba.iso";
    /** Nombre del fichero file_magic que se genera */
    private String file_magic = null;
    /** Tipo de SO en el que se encuentra */
    private String so = null;
    /** Vector que contiene info sobre el avance de la creacion de la iso */
    private Vector info_avance = null;
    /** Subprocesos */
    private static Process p = null;
    private static Process p2 = null;
    private char separator = '\\';
  
    /**
     * Creates a new instance of Mkisofs
     * @param String - Nombre del playlist
     * @param String [] - Conjunto de los nombres de los medios que contendra la imagen
     */
    public Mkisofs(String playlist, String [] medios) {
        this.setPlaylist(playlist);
        this.setMedios(new String [medios.length]);
        for(int i=0; i<medios.length; i++)
            this.getMedios()[i] = medios[i];
        
        this.so = System.getProperty("os.name").toUpperCase(Locale.ENGLISH);
        if(this.so.indexOf(this.SO_WIN) != -1){
            this.cmd_borrar = "del";
            this.so = this.SO_WIN;
            this.separator = '\\';
        }
        if(so.indexOf(this.SO_LINUX) != -1){
            this.cmd_borrar = "rm";
            this.so = this.SO_LINUX;
            this.separator = '/';
        }
        if(so.indexOf(this.SO_MAC) != -1){
            this.cmd_borrar = "rm";
            this.so = this.SO_MAC;
            this.separator = '/';
        }
    }

    /** Dado un path completo devuelve el nombre del archivo al que hace referencia.
     * @param String - Path completo del archivo.
     * @param char - Separador, '\' en WINDOWS, '/' en LINUX o MAC
     * @return String - Nombre del fichero.
     */
    private String file_name(String path, char separator){
        int a = path.lastIndexOf(separator);
        String res = path.substring(a+1);
        return res;
    }
  
    /** Construye una imagen iso a partir de los archivos
     * que se les pasa al constructor.
     * @param String - Nombre de la imagen
     * @return int - 
     *  0 en caso de exito
     *  1 si no se encuentra el ejecutable mkisofs
     *  2 alguno de los archivos no existe y no se ha creado la imagen
     *  3 si 'Permission denied'
     */
    public int mkiso(String imagen, String file_magic) {
        this.p = null;
        this.info_avance = new Vector();
        this.setImagen(imagen);
        this.setFile_magic(file_magic);
        
        //Se genera el archivo file_magic
        FileMagic fm = new FileMagic();
        fm.generate_file_magic(this.getFile_magic());
        
        //Preparando la orden a ejecutar
        String ordenp1 = "mkisofs -J -input-charset utf-8 -r -iso-level 3 -graft-points -V IUPDATE -o " + this.getImagen();
        String ordenp2 = " /" + this.file_name(this.getPlaylist(),this.separator) + "=" + this.getPlaylist();
        ordenp2 = ordenp2 + " /" + this.file_name(this.getFile_magic(),this.separator) + "=" + this.getFile_magic();
        for(int i=0; i<this.getMedios().length; i++)
            ordenp2 = ordenp2 + " media/" + this.file_name(this.getMedios()[i],this.separator) + "=" + this.getMedios()[i];
        
        //Ejecutando la orden
        try {            
            this.p = Runtime.getRuntime().exec(ordenp1+ordenp2);
        } catch (IOException ex) {
            ex.printStackTrace();
            return 1;
        }
        //Procesando la salida devuelta
        //  Se obtiene el stream de salida del programa
        InputStream is = this.p.getErrorStream();
        //  Se prepara un BufferedReader para poder leer la salida m�s comodamente.
        BufferedReader br = new BufferedReader (new InputStreamReader (is));
        try {
            String aux = br.readLine(); 
            // Mientras se haya leido alguna linea 
            while (aux!=null) { 
                if(aux.indexOf("Invalid node") != -1)
                    return 2;
                if(aux.indexOf("Permission denied") != -1)
                    return 3;
                // Se escribe la linea en pantalla 
                System.out.println (aux);
                
                String [] div = aux.split("[ ]+");
                if(div[1].matches("[\\d]+.[\\d]+%")){
                    //div[1].replaceAll("\\p{Punct}","");
                    div[1] = div[1].replaceAll("%","");
                    this.info_avance.addElement(new Vector());
                    ((Vector)this.info_avance.lastElement()).addElement(aux);
                    ((Vector)this.info_avance.lastElement()).addElement(div[1]);
                    ((Vector)this.info_avance.lastElement()).addElement(div[8]);
                }
                // y se lee la siguiente. 
                aux = br.readLine(); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        //Cerramos los flujos
        try{           
            br.close();
            is.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        //eliminamos el file_magic
        if(this.so.indexOf(this.SO_WIN)==-1)
            this.eliminaFile(this.cmd_borrar, this.getFile_magic(), this.so);
        return 0;
    }


    public String getPlaylist() {
        return playlist;
    }

    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }

    public String[] getMedios() {
        return medios;
    }

    public void setMedios(String[] medios) {
        this.medios = medios;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getFile_magic() {
        return file_magic;
    }

    public void setFile_magic(String file_magic) {
        this.file_magic = file_magic;
    }

    /** Elimina un fichero
     * @param String file - Nombre del fichero a eliminar
     */
    public static void eliminaFile(String cmd, String file, String so){
        p2 = null;
        if(so.indexOf(Mkisofs.SO_WIN)==-1)
            try {            
                p2 = Runtime.getRuntime().exec(cmd + " " + file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }

    public Vector getInfo_avance() {
        return info_avance;
    }
    
    /** Para la ejecucion de los procesos
     */
    public void stop(){
        if(this.p != null)
            this.p.destroy();
        if(this.p2 != null)
            this.p2.destroy();
    }
}
