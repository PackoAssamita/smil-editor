/*
 * USBStorageSingleton.java
 *
 * Created on 19 de septiembre de 2007, 18:20
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.util.Locale;

/**
 * Clase que implementa el patron Singleton para USBStorage
 * @author Fco. Jesus Gonzalez Mata
 */
public class USBStorageSingleton {
    
    public static USBStorage usb;
    public static String so;
    public static String SO_WINXP = "WINDOWS XP";
    public static String SO_LINUX = "LINUX";
    public static String SO_MAC = "MAC";
    private String regex_split_path;
    
    /**
     * Creates a new instance of USBStorageSingleton
     */
    public USBStorageSingleton() {
        this.so = System.getProperty("os.name").toUpperCase(Locale.ENGLISH);
        if(so.compareTo(this.SO_WINXP) == 0){
            this.regex_split_path = "\\\\";
            this.so = this.SO_WINXP;
            this.usb = new USBStorageWindows();
        }
        if(so.compareTo(this.SO_LINUX) == 0){
            this.regex_split_path = "/";
            this.so = this.SO_LINUX;
            this.usb = new USBStorageLinux();
        }
        if(so.compareTo(this.SO_MAC) == 0){
            this.regex_split_path = "/";
            this.so = this.SO_MAC;
            this.usb = new USBStorageLinux();
        }
    }
    
}
