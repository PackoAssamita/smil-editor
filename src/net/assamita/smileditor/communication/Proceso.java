/*
 * Proceso.java
 *
 * Created on 18 de septiembre de 2007, 17:40
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Vector;

/**
 * Ejecuta una orden del sistema y se busca la cadena en los
 * streams de error y de salida
 * @author Fco. Jesus Gonzalez Mata
 */
public class Proceso implements Runnable{
    
    Process p; //Proceso que ejecutara la orden
    Thread t; //Hebra para capturar uno de los streams de p.
    
    String orden; //Orden a ejecutar
    String cadena; //Cadena que se buscara en los streams
    String [] found_in_errorstream; //Matches de 'cadena' en el stream de error
    String [] found_in_inputstream; //Matches de 'cadena' en el stream de entrada

    private boolean stream_error = false; //Usado para la sincronizacion de los streams
    
    /** Creates a new instance of Proceso */
    public Proceso() {
    }
    
    /** Lanza la ejecucion del comando especificado. Hay que jugar con las
     * posibles combinaciones. Ej.: 
     *  "umount /dev/sdb1",null,0
     *  "ls -l",null,4
     *  "df -B MB /dev/sdb1","/dev/sdb1",0
     *  "diskpart",null,-1
     * @param String - comando a ejecutar
     * @param String - cadena a buscar en los stream de 'input' y 'error'
     * @param int - columna a extraer de los streams. -1 si se trata de comandos 
     *  windows que necesitan un comportamiento especifico
     */
    public String [] start(String orden, String cadena, int columna){
        
        String [] resultado = null;
        
        this.orden = orden;
        this.cadena = cadena;
        this.t = new Thread(this, "Hebra asociada a la orden " + this.orden);
        
        //Ejecutando la orden
        try {            
            p = Runtime.getRuntime().exec(orden);
            if(this.cadena != null)
                this.t.start();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        
        if(this.cadena!=null || columna>0){
            //Procesando la salida devuelta
            //  Se obtiene el stream de salida del programa
            InputStream is = p.getInputStream();
            //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
            BufferedReader br = new BufferedReader (new InputStreamReader (is));

            if(this.cadena != null)
                this.found_in_inputstream = this.grep(br, this.cadena);
            else
                this.found_in_inputstream = this.columna(br, columna);

            //Cerramos los flujos
            try{           
                br.close();
                is.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            
            while(!this.stream_error && cadena !=null){
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            int a=0, b=0, c=0;
            if(this.found_in_inputstream != null){
                b = this.found_in_inputstream.length;
            }
            c = b;
            if(this.found_in_errorstream != null){
                c = b + this.found_in_errorstream.length;
            }
            
            resultado = new String [c];
            
            for(int i=a; i<b; i++)
                resultado[i] = this.found_in_inputstream[i];
            for(int i=b; i<c; i++)
                resultado[i] = this.found_in_errorstream[i];
            
        }
        
        if(columna==-1){
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader (new InputStreamReader (is));
            this.respuestas_programas_windows(br);
        }
        
        return resultado;
    }
    
    /** Cuerpo de la Hebra
     */
    public void run(){
        //Procesando la salida devuelta
        //  Se obtiene el stream de salida del programa
        InputStream is_error = p.getErrorStream();
        //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
        BufferedReader br_error = new BufferedReader (new InputStreamReader (is_error));

        this.found_in_errorstream = this.grep(br_error,this.cadena);
        
        //Cerramos los flujos
        try{           
            br_error.close();
            is_error.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        this.stream_error = true;
    }
    
    /**
     * Busca el contenido de una cadena en un archivo
     * @param BufferedReader - Stream en donde buscar
     * @param String - Cadena a buscar
     * @return String [] - Lineas que contienen la cadena de
     *  busqueda. null en caso de error o si no existe ninguna.
     */
    public String [] grep(BufferedReader br, String cadena){

        String [] lineas = null;
        Vector v = new Vector();
        String linea = null;
        
        try {
            while((linea=br.readLine()) != null ){
                if(linea.indexOf(cadena) != -1)
                    v.add(linea); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        lineas = new String [v.size()];
        for(int i=0; i<v.size(); i++)
            lineas[i] = (String)v.elementAt(i);

        return lineas;
    }

    /**
     * Busca el contenido de una columna de un stream
     * @param BufferedReader - Stream en donde buscar
     * @param int - Columna a extraer
     * @return String [] - Lineas que contienen la cadena de
     *  busqueda. null en caso de error o si no existe ninguna.
     */
    public String [] columna(BufferedReader br, int columna){

        String [] lineas = null;
        Vector v = new Vector();
        String linea = null;
        
        try {
            while((linea=br.readLine()) != null ){
                String [] aux = linea.split("[ ]+");
                if(columna < aux.length)
                    v.add(aux[columna]); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        lineas = new String [v.size()];
        for(int i=0; i<v.size(); i++)
            lineas[i] = (String)v.elementAt(i);

        return lineas;
    }
    
    /**
     * Busca el contenido de una columna de un stream
     * @param BufferedReader - Stream en donde buscar
     */
    public void respuestas_programas_windows(BufferedReader br){

        String linea = null;
        OutputStream os = p.getOutputStream();
        BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(os));
        
        try {
            while((linea=this.leer_linea(br)) != null ){
                if(linea.indexOf("y presione Entrar cuando ") != -1){
                    bw.newLine();
                    bw.flush();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /** Lee una linea de un stream. Se diferencia de readLine() en
     * que no necesita alcanzar '\n' o '\r'. Si hay datos devuelve los
     * justos antes de bloquearse. Si no hay se bloquea hasta que 
     * haya nuevos datos o se alcance el fin del stream
     * @param BufferesReader - Buffer asociado a un stream del que
     *  se leen los datos
     * @return String - Linea leida o null si fin de stream
     */
    public String leer_linea(BufferedReader br) throws IOException{
        String res = "";
        char [] linea = null;
        char [] caracter = new char [1];
        int max = 4000;
        int i = 0;
        int leido = 0;
        int available = 0;
        boolean seguir = true;
        boolean entrar = true;
        
        
        linea = new char [max];
        
        try {
            i = 0;
            leido = 0;
            int cod = br.read(caracter, 0, 1);
            if(cod == -1){
                seguir = false;
                return null;
            }
            linea[i] = caracter[0];
            i++;
            leido++;
            if(caracter[0] == '\n' || caracter[0] == '\r'){
                i--;
                leido--;
                seguir = false;
            }
            if(leido==max){//Si alcanzamos el tope del buffer
                max = max*2;
                char [] brb = new char [max];
                for(int j=0; j<leido; j++)
                    brb[j] = linea[j];
                linea = brb;
            }
            while(br.ready() && seguir){
            //while(seguir){
                cod = br.read(caracter, 0, 1);
                if(cod == -1)
                    return null;
                linea[i] = caracter[0];
                i++;
                leido++;
                if(caracter[0] == '\n' || caracter[0] == '\r'){
                    i--;
                    leido--;
                    seguir = false;
                    break;
                }
            }
            
            for(i=0; i<leido; i++)
                res = res + linea[i];

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return res;
    }
}
