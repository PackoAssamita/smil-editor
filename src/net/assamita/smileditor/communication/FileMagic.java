/*
 * FileMagic.java
 *
 * Created on 14 de septiembre de 2007, 8:53
 *
  
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

/**
 * Genera archivos FileMagic a partir de una secuencia numerica de inicio
 * @author Fco. Jesus Gonzalez
 */
public class FileMagic {
    
    String secuencia = "311025225125025225525631102503" + 
        "118257251252256255311025031182562992572513120" + 
        "250256252255310925031202562522572502552512532562572502553110210";
    
    /**
     * Creates a new instance of FileMagic
     */
    public FileMagic() {
    }
    
    
    /**
     * Genera el file_magic a partir de una secuencia de numeros
     * @param String file - Path y nombre del archivo que se crea
     * @return int - 
     *  0 si exito.
     *  1 si error al crear el fichero.
     *  2 si error al escribir en el fichero.
     */
    public int generate_file_magic(String file){
        
        FileInputStream fin;
        DataInputStream in;
        FileOutputStream fout;
        DataOutputStream out;
        byte [] aux = null;
        
        try {

            fout = new FileOutputStream(file);
            out = new DataOutputStream(fout);
            try {

                int i = 0;
                Vector v = new Vector();
                while(i<this.secuencia.length()){
                    int x = Integer.parseInt(this.secuencia.substring(i,i+1));
                    v.addElement(Byte.valueOf(this.secuencia.substring(i+1,i+x+1)));
                    i = i+x+1;
                }
                
                aux = new byte [v.size()];
                for(i=0; i<v.size(); i++)
                    aux[i] = ((Byte)v.elementAt(i)).byteValue();
                
                out.write(aux);
                out.flush();
                out.close();
                fout.close();

            } catch (IOException ex) {
                ex.printStackTrace();
                return 2;
            }
        
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    /** Elimina un fichero
     * @param String file - Nombre del fichero a eliminar
     */
    public static void eliminaFile(String file){
        Process p2 = null;
        try {            
            p2 = Runtime.getRuntime().exec("rm " + file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
