/*
 * BurnISO.java
 *
 * Created on 13 de septiembre de 2007, 13:06
 *
  
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Quema una imagen iso en un disco o un dispositivo externo
 * @author Fco. Jesus Gonzalez
 */
public class BurnISO implements Runnable{
    
    String [] recorders; //Num de grabadores encontrados
    int index_recorders = -1; //Indice del grabador seleccionado
    
    private String imagen = "prueba.iso"; //Nombre por defecto de la imagen 
    Process p1; //
    private static Process p2 = null;
    Thread t; //Hebra encargada de capturar uno de los stream de salida del programa
    boolean stream_in = false; //Flujo de salida estandar de la aplicacion que se ejecutara
    boolean stream_error = false; //Flujo de salida de errores de la aplicacion que se ejecutara
    int codigo = -1;

    //Patrones de reconocimiento de salidas
    String orden_scanbus = "cdrecord -scanbus";
    String parte1 = "(\\t\\d,\\d,\\d\\t[ ]*[\\d]+\\p{Punct})"; // "\t0,0,0\t  0) "
    String parte2 = "([ ]+(\')([ \\w\\p{Punct}]*)(\'))"; // " 'LITE-ON '"
    //String parte3 = "([ \\w\\p{Punct}]*)";  Removable CD-ROM
    String parte3 = "[ \\p{Punct}]*Removable CD-ROM";
    
    /** Vector que contiene info sobre el avance de la creacion de la iso */
    private Vector info_avance = null;
    
    /** Creates a new instance of BurnISO */
    public BurnISO() {
        this.t = new Thread(this, "Hebra BurnISO");
    }

    
    /** Quema una imagen iso en un disco
     * @param String - Nombre de la imagen
     * @return int - 
     *  0 si exito
     *  1 si no se encuentra cdrecord
     *  2 si no existen grabadores
     *  3 si "no CD/DVD-Recorder or unsupported CD/DVD-Recorder"
     *  4 si "No disk / Wrong disk"
     *  5 si no se encuentra la imagen a grabar
     */
    public int iso2disk(String imagen){
        int existen = 0;
        this.codigo = -1;
        this.setImagen(imagen);
        this.info_avance = new Vector();
        
        if(this.index_recorders == -1)
            existen = scanbus();
        
        if(existen != -2){
        
            //Se extrae el dispositivo grabador
            String [] res = this.recorders[this.index_recorders].split("\t");
            String dev = res[1];
            
            //Preparando la orden a ejecutar
            String ordenp1 = "cdrecord -v -blank=fast -eject speed=16 dev=" + dev + " fs=16m -data " + this.getImagen();
            //String ordenp1 = "cdrecord -blank=fast -eject dev=0,0,0";//-log-file log.txt 

            //Ejecutando la orden
            try {            
                p1 = Runtime.getRuntime().exec(ordenp1);
                this.t.start();
            } catch (IOException ex) {
                ex.printStackTrace();
                return 1;
            }
            
            //Procesando la salida devuelta
            //  Se obtiene el stream de salida del programa
            InputStream is = p1.getInputStream();
            //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
            BufferedReader br = new BufferedReader (new InputStreamReader (is));
            try {
                String linea_in = null;
                linea_in = br.readLine();
                // Mientras se haya leido alguna linea 
                while (linea_in!=null) { 
                    // Se escribe la linea en pantalla 
                    System.out.println ("out: " + linea_in); 
                    
                    String [] div = linea_in.split("[ ]+");
                    if(div[div.length-1].matches("[\\d]+.[\\d]+x.") &&
                        div[div.length-2].matches("[\\d]+%\\]")){
                        
                        this.info_avance.addElement(new Vector());
                        //linea completa
                        ((Vector)this.info_avance.lastElement()).addElement(linea_in);
                        //MB actual
                        ((Vector)this.info_avance.lastElement()).addElement(div[2]);
                        //MB totales
                        ((Vector)this.info_avance.lastElement()).addElement(div[4]);
                        //% Fifo
                        div[8] = div[8].replaceAll("%\\)","");
                        ((Vector)this.info_avance.lastElement()).addElement(div[8]);
                        //% Buffer
                        div[10] = div[10].replaceAll("%\\]","");
                        ((Vector)this.info_avance.lastElement()).addElement(div[10]);
                        // Speed
                        ((Vector)this.info_avance.lastElement()).addElement(div[11]);
                    }
                    
                    // y se lee la siguiente. 
                    linea_in = br.readLine(); 
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            } 
            
            //Cerramos los flujos
            try{           
                br.close();
                is.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else
            this.codigo = 2;
        
        while(!this.stream_error){ //sincronizacion con el otro stream
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        if(this.codigo == -1)
            this.codigo = 0;
        
        return this.codigo;
    }


    public void run() {
        //Procesando la salida devuelta
        //  Se obtiene el stream de salida del programa
        InputStream is_error = p1.getErrorStream();
        //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
        BufferedReader br_error = new BufferedReader (new InputStreamReader (is_error));

        try {
            String linea_error = null;
            linea_error = br_error.readLine();
            // Mientras se haya leido alguna linea 
            while (linea_error!=null) { 
                if(linea_error.indexOf("no CD/DVD-Recorder or unsupported CD/DVD-Recorder") != -1)
                    this.codigo = 3;
                if(linea_error.indexOf("No disk / Wrong disk") != -1)
                    this.codigo = 4;  
                if(linea_error.indexOf("Try to load media by hand") != -1)
                    this.codigo = 4;      
                if(linea_error.indexOf("Cannot open ") != -1)
                    this.codigo = 5;      
                // Se escribe la linea en pantalla 
                System.out.println ("   error: " + linea_error); 
                // y se lee la siguiente. 
                linea_error = br_error.readLine(); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
        
        //Cerramos los flujos
        try{           
            br_error.close();
            is_error.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        this.stream_error = true;
    }

    

    
    
  /** Devuelve la lista con los posibles dispositivos grabadores
   * @return String [] - Lista con los dispositivos grabadores, null
   *  si no existen dispositivos grabadores
   */
  public String [] getDeviceList(){

      if(this.scanbus()==-2)
          return null;

      String [] res = new String [this.recorders.length];
      for(int i=0; i<this.recorders.length; i++){
          String aux = this.recorders[i];
          int a = aux.indexOf("'");
          int b = aux.indexOf("'", a+1);
          int c = aux.indexOf("'", b+1);
          int d = aux.indexOf("'", c+1);
          res[i] = aux.substring(a,b)+aux.substring(c,d);
          res[i] = res[i].replaceAll("\'"," ");
      }
      return res;
  }
  
    /** Establece el numero de grabador
     * @param int - Indice del grabador 
     */
    public void setBurntDevice(int index){
        this.index_recorders = index;
    }

    
    /** Scanea el bus scsi para averiguar los posibles
     * dispositivos grabadores. Inicializa la variable
     * this.recorders.
     * @return int - 
     *  -1 si no se encuentra el ejecutable cdrecord
     *  -2 si no encuentra ningun grabador
     *  n numero de grabadores que encuentra
     */    
    public int scanbus() {
        Process p = null;  
        Vector v = new Vector();

        //Ejecutando la orden
        try {            
            p = Runtime.getRuntime().exec(orden_scanbus);
        } catch (IOException ex) {
            ex.printStackTrace();
            return -1;
        }
        
        //Procesando la salida devuelta
        //  Se obtiene el stream de salida del programa
        InputStream is = p.getInputStream();
        //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
        BufferedReader br = new BufferedReader (new InputStreamReader (is));

        try {
            String aux = br.readLine(); 
            // Mientras se haya leido alguna linea 
            while (aux!=null) { 
                if((aux).matches(parte1+parte2+"{3,}"+parte3)){
                    //this.recorders[this.recorders.length] = aux;
                    v.addElement(aux);
                }
                // Se escribe la linea en pantalla 
                //System.out.println (aux); 
                // y se lee la siguiente. 
                aux = br.readLine(); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
        
        //Cerramos los flujos
        try{           
            br.close();
            is.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        //Inicializamos recorders una vez sabemos el numero exacto de grabadores
        if(v.size()>0){
            this.recorders = new String [v.size()];
            for (int i=0; i<v.size(); i++)
                this.recorders[i] = (String)v.elementAt(i);
            this.index_recorders = 0; //establecemos el grabador por defecto
            return this.recorders.length;
        }
        else
            return -2;
    }

    /** Elimina un fichero
     * @param String file - Nombre del fichero a eliminar
     */
    public static void eliminaFile(String file){
        p2 = null;
        try {            
            p2 = Runtime.getRuntime().exec("rm " + file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
    public Vector getInfo_avance() {
        return info_avance;
    }
    
    /** Para la grabacion en curso
     */
    public void stop(){
        if(this.p1 != null){
            try {            
                Runtime.getRuntime().exec("cdrecord -abort");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            this.p1.destroy();
        }
        if(this.p2 != null)
            this.p2.destroy();
    }

}
