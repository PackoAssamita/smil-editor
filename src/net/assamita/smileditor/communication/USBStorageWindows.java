/*
 * USBStorageWindows.java
 *
 * Created on 19 de septiembre de 2007, 18:15
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Especificacion de USBStorage
 * @author Fco. Jesus Gonzalez Mata
 */
public class USBStorageWindows extends USBStorage{
    
    String macro = "C:\\windows\\temp\\macro.bat";
    String macro_size = "C:\\windows\\temp\\macro.size.bat";
    String result_size = "C:\\windows\\temp\\result.txt";
    
    /**
     * Creates a new instance of USBStorageWindows
     */
    public USBStorageWindows() {
        super();
    }


    
    /** Copia unos archivos a un dispositivo de 
     * almacenamiento conectado a un usb
     * @param index - Dispositivo en donde copiar
     * @return int - 0 si exito
     *  -1 si no existen dispositivos o no estan monetados
     *  -2 si no existe suficiente espacio para grabar los ficheros
     */
    public int copy2usb(int index, String playlist, String file_magic, String[] medios) {

        //Obtenemos los dispositivos si no se han obtenido antes
        if(this.devices.size() == 0)
            this.getUSBDeviceList();
        
        if(this.devices.size() > 0){
            Vector v = (Vector) this.devices.elementAt(index);
            
            //Indicamos que empezamos a copiar
            this.isFinish = false;
            
            //Comprobamos si los datos caben en el dispositivo
            Vector lineas = new Vector();
            String cmd = "dir " + playlist + " " + file_magic + " ";
            for(int i=0; i<medios.length; i++)
                cmd = cmd + medios[i] + " ";
            cmd = cmd + " > " + this.result_size;
            lineas.addElement(cmd);
            this.escribir_en_fichero(this.macro_size, lineas);
            Proceso p0 = new Proceso();
            p0.start(this.macro_size,null,-1);
            //buscamos el tamanio de cada archivo en 
            String [] resultados = this.grep(this.result_size,"archivos");
            if(resultados!=null){
                String [] aux = this.columna(resultados,3);
                float total = 0;
                for(int i=0; i<aux.length; i++){
                    aux[i] = aux[i].replaceAll("\\.","");
                    total += Float.parseFloat(aux[i]);
                }
                total = total / (1024*1024);
                String capacidad = (String)v.lastElement();
                capacidad = capacidad.replaceAll("MB","");
                if(Float.parseFloat(capacidad) < total)
                    return -2;
            }
            
            //Creamos una macro que se ejecutara despues
            FileMagic fm = new FileMagic();
            fm.generate_file_magic(file_magic);
            lineas = new Vector();
            lineas.addElement("format " + ((String)v.elementAt(0)) + ": /FS:FAT32 /V:IUPDATE /Q");
            lineas.addElement("mkdir " + ((String)v.elementAt(0)) + ":\\media");
            lineas.addElement("copy " + playlist + " " + ((String)v.elementAt(0)) + ":");
            lineas.addElement("copy " + file_magic + " " + ((String)v.elementAt(0)) + ":");
            for(int i=0; i<medios.length; i++)
                lineas.addElement("copy " + medios[i] + " " + ((String)v.elementAt(0)) + ":\\media");
            this.escribir_en_fichero(this.macro,lineas);

            //Formateamos el dispositivo
            Proceso p = new Proceso();
            p.start(this.macro,null,-1);
            
            //Indicamos que empezamos a copiar
            this.isFinish = true;
            
            return 0;
        }
        else
            return -1;
    }
    
    /** Escribe las lineas del vector en el fichero
     * @param String - Nombre del fichero
     * @param Vector - Vector que contiene las lineas
     */
    public void escribir_en_fichero(String fichero, Vector lineas){
        File archivo = null;
        FileWriter fw = null;
        BufferedWriter bw = null;
        
        try {
            archivo = new File(fichero);
            fw = new FileWriter(archivo);
            bw = new BufferedWriter(fw);
            
            for(int i=0; i<lineas.size(); i++){
                bw.write((String)lineas.elementAt(i));
                bw.newLine();
            }
            
            bw.flush();
            bw.close();
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /** Devuelve la lista con los posibles dispositivos de almacenamiento USB
    * @return String [] - Lista con los dispositivos grabadores, null
    *  si no existen dispositivos 
    */
    public String[] getUSBDeviceList() {
        
        String [] res = null;
        
        DiskPartProcess dpp = new DiskPartProcess();
        dpp.start();
        System.out.println();

        dpp.write("list volume");
        this.devices = dpp.getDevices();
        
        if(this.devices.size() > 0){
            res = new String [this.devices.size()];
            for(int i=0; i<this.devices.size(); i++){
                res[i] = (String)((Vector)this.devices.elementAt(i)).elementAt(0);
                res[i] = res[i] + " " + (String)((Vector)this.devices.elementAt(i)).elementAt(1);
            }
        }
        dpp.write("exit");
        return res;
    }
    
}
