/*
 * USBStorageLinux.java
 *
 * Created on 19 de septiembre de 2007, 18:14
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;

/**
 * Especificacion de USBStorage
 * @author Fco. Jesus Gonzalez Mata
 */
public class USBStorageLinux extends USBStorage{
    
    /**
     * Creates a new instance of USBStorageLinux
     */
    public USBStorageLinux() {
    }

    /** Copia unos archivos a un dispositivo de 
     * almacenamiento conectado a un usb
     * @param index - Dispositivo en donde copiar
     * @return int - 0 si exito
     *  -1 si no existen dispositivos o no estan monetados
     *  -2 si no existe suficiente espacio para grabar los ficheros
     */
    public int copy2usb(int index, String playlist, String file_magic, String [] medios){

        
        Proceso p1 = new Proceso();
        
        //Obtenemos los dispositivos si no se han obtenido antes
        if(this.devices.size() == 0)
            this.getUSBDeviceList();
        
        if(this.devices.size() > 0){
            Vector v = (Vector) this.devices.elementAt(index);

            //Comprobamos si los datos caben en el dispositivo
            String orden = "ls -l";
            for(int i=0; i<medios.length; i++)
                orden = orden + " " + medios[i];
            orden = orden + " " + playlist;
            //++++orden = orden + " " + file_magic;
            String [] aux = p1.start(orden,null,4);
            float total = 0;
            for(int i=0; i<aux.length; i++)
                total += Float.parseFloat(aux[i]);
            total = total / (1024*1024);
            String capacidad = (String)v.lastElement();
            capacidad = capacidad.replaceAll("MB","");
            if(Float.parseFloat(capacidad) < total)
                return -2;

            //Formateamos el dispositivo
            p1.start("umount " + ((String)v.elementAt(0)),null,0);
            p1.start("mkfs -V -t vfat " + ((String)v.elementAt(0)),null,0);

            //Escribimos el contenido en el dispositivo
            p1.start("mount " + ((String)v.elementAt(0)),null,0);
            p1.start("mkdir " + ((String)v.elementAt(1)) + "/media",null,0);
            FileMagic fm = new FileMagic();
            //fm.generate_file_magic(((String)v.elementAt(1)) + "/" + file_magic);
            fm.generate_file_magic(file_magic);
            p1.start("cp " + playlist + " " + ((String)v.elementAt(1)),null,0);
            p1.start("cp " + file_magic + " " + ((String)v.elementAt(1)),null,0);
            for(int i=0; i<medios.length; i++)
                p1.start("cp " + medios[i] + " " + ((String)v.elementAt(1)) + "/media",null,0);
            //p1.start("umount " + ((String)v.elementAt(0)),null,0);

            return 0;
        }
        else
            return -1;
    }
    
    /** Devuelve la lista con los posibles dispositivos de almacenamiento USB
    * @return String [] - Lista con los dispositivos grabadores, null
    *  si no existen dispositivos 
    */
    public String [] getUSBDeviceList(){

        String [] res = null;
        String [] lineas = this.grep("/etc/mtab","vfat");
        
        if(lineas != null){

            res = new String [lineas.length];
            for(int i=0; i<lineas.length; i++){
                //Obtenemos la informacion relevante del archivo
                String [] aux = lineas[i].split(" ");
                this.devices.addElement(new Vector());
                Vector row = (Vector) this.devices.elementAt(i);
                row.addElement(aux[0]);
                row.addElement(aux[1]);
                row.addElement(aux[2]);
                res[i] = aux[0] + " " + aux[1];
                //Obtenemos otra informacion como el tamaño disponible
                String dev = aux[0];
                Proceso p2 = new Proceso();
                String [] res_p2 = p2.start("df -B MB "+dev, dev, 0);
                aux = res_p2[0].split("[ ]+");
                row.addElement(aux[1]);
                res[i] = res[i] + " " + aux[1];
            }
        }
        
        return res;
    }
    
}
