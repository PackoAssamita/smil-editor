/*
 * USBStorage.java
 *
 * Created on 18 de septiembre de 2007, 17:12
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;


/**
 * Clase abstracta para el grabado en dispositivos USB
 * @author Fco. Jesus Gonzalez Mata
 */
public abstract class USBStorage {
    
    /** Dispositivos de almacenamiento USB encontrados */
    protected Vector devices; 
    protected boolean isFinish = false;
    
    /**
     * Creates a new instance of USBStorage
     */
    public USBStorage() {
        this.devices = new Vector();
    }
    
    /** Copia la estructura de la imagen a un dispositivo de 
     * almacenamiento conectado a un usb
     * @param index - Dispositivo en donde copiar
     * @return int - 0 si exito
     *  -1 si no existen dispositivos o no estan montados
     *  -2 si no existe suficiente espacio para grabar los ficheros
     */
    public abstract int copy2usb(int index, String playlist, String file_magic, String [] medios);
    
    
    /** Devuelve la lista con los posibles dispositivos de almacenamiento USB
    * @return String [] - Lista con los dispositivos grabadores, null
    *  si no existen dispositivos 
    */
    public abstract String [] getUSBDeviceList();

    
    public boolean isIsFinish() {
        return isFinish;
    }

    /**
     * Busca el contenido de una cadena en un archivo
     * @param String - Nombre del fichero en donde buscar
     * @param String - Cadena a buscar
     * @return String [] - Lineas que contienen la cadena de
     *  busqueda. null en caso de error o si no existe ninguna.
     */
    protected String [] grep(String fichero, String cadena){
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String [] lineas = null;
        Vector v = new Vector();
        String linea = null;
        
        try {
            archivo = new File(fichero);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            try {
                while((linea=br.readLine()) != null ){
                    if(linea.indexOf(cadena) != -1)
                        v.add(linea); 
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            lineas = new String [v.size()];
            for(int i=0; i<v.size(); i++)
                lineas[i] = (String)v.elementAt(i);
        
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            if(fr != null)
                try {
                    fr.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
        }
        if(v.size()>0)
            return lineas;
        else
            return null;
    }
    
    /**
     * Busca el contenido de una columna de un stream
     * @param BufferedReader - Stream en donde buscar
     * @param int - Columna a extraer
     * @return String [] - Lineas que contienen la cadena de
     *  busqueda. null en caso de error o si no existe ninguna.
     */
    public String [] columna(BufferedReader br, int columna){

        String [] lineas = null;
        Vector v = new Vector();
        String linea = null;
        
        try {
            while((linea=br.readLine()) != null ){
                String [] aux = linea.split("[ ]+");
                if(columna < aux.length)
                    v.add(aux[columna]); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        lineas = new String [v.size()];
        for(int i=0; i<v.size(); i++)
            lineas[i] = (String)v.elementAt(i);

        return lineas;
    }
    
    
    /**
     * Busca el contenido de una columna de un stream
     * @param String - Conjunto de String en donde buscar
     * @param int - Columna a extraer
     * @return String [] - Lineas que contienen la cadena de
     *  busqueda. null en caso de error o si no existe ninguna.
     */
    public String [] columna(String [] in, int columna){

        String [] lineas = null;
        Vector v = new Vector();
        String linea = null;
        
        for(int i=0; i<in.length; i++){
            String [] aux = in[i].split("[ ]+");
            if(columna < aux.length)
                v.add(aux[columna]); 
        }

        lineas = new String [v.size()];
        for(int i=0; i<v.size(); i++)
            lineas[i] = (String)v.elementAt(i);

        return lineas;
    }
}
