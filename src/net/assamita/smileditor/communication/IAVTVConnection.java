/*
 * IAVTVConnection.java
 *
 * Created on 7 de septiembre de 2007, 13:29
 *
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Locale;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Establece la comunicacion entre la escaleta y la TVStation
 * @author Fco. Jesus Gonzalez
 */
public class IAVTVConnection implements Runnable{

    //Variables globales
    public static String ACTUALIZAR = "ACTUALIZAR";
    public static String MANDAR = "MANDAR";
    public static String SO_WIN = "WINDOWS";
    public static String SO_LINUX = "LINUX";
    public static String SO_MAC = "MAC";
    
    private String imagen = "prueba.iso"; //Nombre por defecto de la imagen
    private String user = "user"; 
    private String dir = "/tmp/"; //Directorio por defecto en donde se guardarn los contenidos
    private String comando_remoto = "ls"; //Comando por defecto que se ejecutara
    private String passwd = null; //Password correspondiente al usuario 'user' 
    private String host = null; //Direccion IP
    private Process p = null;
    
    private String so = null; //Tipo del Sistema Operativo en el que corre la aplicacion

    //Variables que cambian dependiendo del SO
    private String regex_split_path; //Expresion regular usada para dividir un path segun '\' o '/'
    private String scp_program; //Comando usado para SCP
    private String ssh_program; //Comando usado para SSH
    
    private Thread t = null;
    private int codigo = -1; 

    private boolean stream_error = false;
    
    /** Vector que contiene info sobre el avance de la creacion de la iso */
    private Vector info_avance = null;
    
    /**
     * Creates a new instance of IAVTVConnection
     * @param String - Nombre del SO
     */
    public IAVTVConnection(String so) {
        String so2 = so;
        
        if(so2 == null){ //averiguamos el so que tenemos
            this.so = System.getProperty("os.name").toUpperCase(Locale.ENGLISH);
            so2 = this.so;
        }
        if(so2.indexOf(this.SO_WIN) != -1){
            this.regex_split_path = "\\\\";
            this.so = this.SO_WIN;
        }
        if(so2.indexOf(this.SO_LINUX) != -1){
            this.regex_split_path = "/";
            this.so = this.SO_LINUX;
        }
        if(so2.indexOf(this.SO_MAC) != -1){
            this.regex_split_path = "/";
            this.so = this.SO_MAC;
        }
        this.setScp_program("pscp");
        this.setSsh_program("plink");
        //this.t = new Thread(this, "Hebra IAVTVConnection");
    }
    

    /** Ejecuta una orden
     * @param String - User
     * @param String - Password 
     * @param String - Host
     * @param String - Orden a enviar: IAVTVConnection.ACTUALIZAR ,  IAVTVConnection.MANDAR
     * @return int - -2 si la opcion no existe
     */
    public int orden(String user, String passwd, String host, String opcion) {
        
        this.setUser(user);
        this.setPasswd(passwd);
        this.setHost(host);
        
        String comando_ejecutar = null;
        
        //Construimos la orden que se va a ejecutar
        if(opcion.compareTo(this.ACTUALIZAR)==0){
            comando_ejecutar = this.getSsh_program() + " -ssh -pw " + this.getPasswd() + " " + this.getUser() 
            + "@" + this.getHost() + " " + this.getComando_remoto();
            return ejecutar(comando_ejecutar);
        }
        if(opcion.compareTo(this.MANDAR)==0){
            String [] des = this.getImagen().split(this.regex_split_path);
            comando_ejecutar = this.getScp_program() + " -pw " + this.getPasswd() + " " + this.getImagen() 
                + " " + this.getUser() + "@" + this.getHost() + ":" + this.getDir() + des[des.length-1];
            return ejecutar(comando_ejecutar);
        }
        return -1;
    }

    /** Ejecuta una orden
     * @param String - Orden a ejecutar
     * @return int -
     *  0 si exito.
     *  2 si "Unable to open connection".
     *  3 si "No route to host".
     *  4 si "Access denied"
     */
    private int ejecutar(String comando_ejecutar) {
        
        this.info_avance = new Vector();
        
        //Ejecutando la orden
        try {            
            p = Runtime.getRuntime().exec(comando_ejecutar);
            this.t = new Thread(this, "Hebra IAVTVConnection");
            this.t.start();
        } catch (IOException ex) {
            ex.printStackTrace();
            return 1;
        }
        //Procesando la salida devuelta
        //  Se obtiene el stream de salida del programa
        InputStream is = p.getInputStream();
        //OutputStream os = p_actualizar.getOutputStream();
        //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
        BufferedReader br = new BufferedReader (new InputStreamReader (is));
        //BufferedWriter bw = new BufferedWriter (new OutputStreamWriter (os));
        try {
            String aux = br.readLine(); 
            // Mientras se haya leido alguna linea 
            while (aux!=null) { 
                // Se escribe la linea en pantalla 
                System.out.println ("input:" + aux); 
                
                if(aux.indexOf("IUPDATE.iso")!=-1){
                    String [] div = aux.split("[ ]+");

                    //div[1].replaceAll("\\p{Punct}","");
                    div[11] = div[11].replaceAll("%","");
                    this.info_avance.addElement(new Vector());
                    ((Vector)this.info_avance.lastElement()).addElement(aux);
                    ((Vector)this.info_avance.lastElement()).addElement(div[11]);
                    ((Vector)this.info_avance.lastElement()).addElement(div[9]);
                    ((Vector)this.info_avance.lastElement()).addElement(div[5] + " " + div[6]);
                }
                
                // y se lee la siguiente. 
                aux = br.readLine(); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }         
        
        //Se cierran los flujos
        try {
            br.close();
            is.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        while(!this.stream_error){ //esperamos a que termine la hebra
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        if(this.codigo == -1)
            this.codigo = 0;
        
        return this.codigo;
    }
    
    /** Cuerpo principal de la hebra
     */
    public void run() {
        //Procesando la salida devuelta
        //  Se obtiene el stream de salida del programa
        InputStream is_error = p.getErrorStream();
        OutputStream os_error = p.getOutputStream();
        //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
        BufferedReader br_error = new BufferedReader (new InputStreamReader (is_error));
        BufferedWriter bw_error = new BufferedWriter (new OutputStreamWriter (os_error));

        try {
            String linea_error = null;
            //+++linea_error = br_error.readLine();
            linea_error = this.leer_linea(br_error);
            
            // Mientras se haya leido alguna linea 
            while (linea_error!=null) { 
                if(linea_error.indexOf("Store key in cache") != -1){
                    bw_error.write('y');
                    bw_error.newLine();
                    bw_error.flush();
                }
                if(linea_error.indexOf("Unable to open connection") != -1){
                    this.codigo = 2;
                    break;
                }
                if(linea_error.indexOf("No route to host") != -1){
                    this.codigo = 3;
                    break;
                }
                if(linea_error.indexOf("Access denied") != -1){
                    this.codigo = 4;
                    break;
                }
                
                // Se escribe la linea en pantalla 
                System.out.println ("   error: " + linea_error); 
                // y se lee la siguiente. 
                linea_error = this.leer_linea(br_error);

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        //Cerramos los flujos
        try{           
            br_error.close();
            is_error.close();
            bw_error.close();
            os_error.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        
        this.stream_error = true;
    }
  
    /** Lee una linea de un stream. Se diferencia de readLine() en
     * que no necesita alcanzar '\n' o '\r'. Si hay datos devuelve los
     * justos antes de bloquearse. Si no hay se bloquea hasta que 
     * haya nuevos datos o se alcance el fin del stream
     * @param BufferesReader - Buffer asociado a un stream del que
     *  se leen los datos
     * @return String - Linea leida o null si fin de stream
     */
    public String leer_linea(BufferedReader br) throws IOException{
        String res = "";
        char [] linea = null;
        char [] caracter = new char [1];
        int max = 200;
        int i = 0;
        int leido = 0;
        int available = 0;
        boolean seguir = true;
        boolean entrar = true;
        
        
        linea = new char [max];
        
        try {
            i = 0;
            leido = 0;
            int cod = br.read(caracter, 0, 1);
            if(cod == -1){
                seguir = false;
                return null;
            }
            linea[i] = caracter[0];
            i++;
            leido++;
            if(caracter[0] == '\n' || caracter[0] == '\r'){
                i--;
                leido--;
                seguir = false;
            }
            while(br.ready() && seguir){
            //while(seguir){
                cod = br.read(caracter, 0, 1);
                if(cod == -1)
                    return null;
                linea[i] = caracter[0];
                i++;
                leido++;
                if(caracter[0] == '\n' || caracter[0] == '\r'){
                    i--;
                    leido--;
                    seguir = false;
                    break;
                }
            }
            
            for(i=0; i<leido; i++)
                res = res + linea[i];

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return res;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getComando_remoto() {
        return comando_remoto;
    }

    public void setComando_remoto(String comando_remoto) {
        this.comando_remoto = comando_remoto;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getScp_program() {
        return scp_program;
    }

    public void setScp_program(String scp_program) {
        this.scp_program = scp_program;
    }

    public String getSsh_program() {
        return ssh_program;
    }

    public void setSsh_program(String ssh_program) {
        this.ssh_program = ssh_program;
    }

    public Vector getInfo_avance() {
        return info_avance;
    }

    /** Para la ejecucion del programa
     */
    public void stop() {
        if(this.p != null)
            this.p.destroy();
    }
}
