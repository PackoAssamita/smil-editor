/*
 * DiskPartProcess.java
 *
 * Created on 20 de septiembre de 2007, 9:05
 
  
  (C) Copyright 2007 Francisco Jose Moreno LLorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>


    SmilEditor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SmilEditor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SmilEditor.  If not, see <http://www.gnu.org/licenses/>.

 */

package net.assamita.smileditor.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Vector;

/**
 * Clase encargada de interactuar con el programa DiskPart de Windows
 * @author Fco. Jesus Gonzalez
 */
public class DiskPartProcess implements Runnable{
    
    private Thread t;
    private Process p;
    private Vector devices;
    private boolean ready = false;
    
    /** Creates a new instance of DiskPartProcess */
    public DiskPartProcess() {
        this.t = new Thread(this,"Hebra DiskPartProcess");
        this.devices = new Vector();
    }
    
    /** Comienza la ejecucion del proceso
     * @return int - 0 si exito, -1 si error
     */
    public int start(){
        //Ejecutando la orden
        try {            
            p = Runtime.getRuntime().exec("diskpart");
            this.t.start();
        } catch (IOException ex) {
            ex.printStackTrace();
            return -1;
        }
        while(!this.isReady()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        return 0;
    }
    
    /** Cuerpo principal de la hebra
     */
    public void run(){
        //  Se obtiene el stream de salida del programa
        InputStream is = p.getInputStream();
        //  Se prepara un BufferedReader para poder leer la salida mas comodamente.
        BufferedReader br = new BufferedReader (new InputStreamReader (is));
        try {
            String aux = leer_linea(br); 
            // Mientras se haya leido alguna linea 
            while (aux!=null) { 
                if(aux.indexOf("Extra") != -1){
                    this.devices.addElement(new Vector());
                    String [] div = aux.split("[ ]+");
                    ((Vector)this.devices.lastElement()).addElement(div[3]);
                    ((Vector)this.devices.lastElement()).addElement(div[div.length-2] + " " + div[div.length-1]);
                }
                if(aux.indexOf("DISKPART> ") != -1){
                    this.setReady(true);
                }
                // Se escribe la linea en pantalla 
                System.out.println ("input:" + aux); 
                // y se lee la siguiente. 
                aux = leer_linea(br); 
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }         
        
        //Se cierran los flujos
        try {
            br.close();
            is.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /** Escribe una cadena en el stream del programa
     * @param String - cadena a escribir
     */
    public void write(String cadena){
        
        while(!this.isReady()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        this.setReady(false);
        
        OutputStream os = p.getOutputStream();
        BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(os));
        try {
            bw.write(cadena);
            bw.newLine();
            bw.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        while(!this.isReady() && cadena.compareTo("exit")!=0){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    /** Lee una linea de un stream. Se diferencia de readLine() en
     * que no necesita alcanzar '\n' o '\r'. Si hay datos devuelve los
     * justos antes de bloquearse. Si no hay se bloquea hasta que 
     * haya nuevos datos o se alcance el fin del stream
     * @param BufferesReader - Buffer asociado a un stream del que
     *  se leen los datos
     * @return String - Linea leida o null si fin de stream
     */
    public String leer_linea(BufferedReader br) throws IOException{
        String res = "";
        char [] linea = null;
        char [] caracter = new char [1];
        int max = 200;
        int i = 0;
        int leido = 0;
        int available = 0;
        boolean seguir = true;
        boolean entrar = true;
        
        
        linea = new char [max];
        
        try {
            i = 0;
            leido = 0;
            int cod = br.read(caracter, 0, 1);
            if(cod == -1){
                seguir = false;
                return null;
            }
            linea[i] = caracter[0];
            i++;
            leido++;
            if(caracter[0] == '\n' || caracter[0] == '\r'){
                i--;
                leido--;
                seguir = false;
            }
            while(br.ready() && seguir){
            //while(seguir){
                cod = br.read(caracter, 0, 1);
                if(cod == -1)
                    return null;
                linea[i] = caracter[0];
                i++;
                leido++;
                if(caracter[0] == '\n' || caracter[0] == '\r'){
                    i--;
                    leido--;
                    seguir = false;
                    break;
                }
            }
            
            for(i=0; i<leido; i++)
                res = res + linea[i];

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return res;
    }

    public Vector getDevices() {
        return devices;
    }

    public boolean isReady() {
        return ready;
    }

    public synchronized void setReady(boolean ready) {
        this.ready = ready;
    }

}
